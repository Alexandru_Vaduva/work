<?php
/*
error_reporting(E_ALL); 
ini_set('display_errors','1'); 

include "test.php";

global $projName;
*/
echo <<< END

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Yocto Web Hob</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
<!--remove and use @import in css in production-->
<link href="../assets/css/bootstrap.css" rel="stylesheet">
<!--remove and use @import in css in production-->
<link href="css/yocto.css" rel="stylesheet">
<link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">
<!--[if !IE 7]>
<style type="text/css">
#wrap {display:table;height:100%}
</style>
<![endif]-->

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="../assets/ico/favicon.ico">

</head>

<body>

<div id="wrap">

	<!--HEADER-->
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">	
 				<a class="brand" href="home.php?page=index_dashboard">&nbsp;</a>
			
				<ul class="nav" style="text-align:center;">
					<!--SET THE ACTIVE SECTION by adding class="active"-->
					<li><a href="home.php?page=builds" class="icon-builds">Builds</a></li>
					<li class="active"><a href="home.php?page=projects" class="icon-projects">Projects</a></li>
					<!-- <li><a href="home.php?page=groups" class="icon-groups">Groups</a></li> -->
				</ul>
		
			
             <!--Top Right Tools-->
             <div id="top-right-tools">
           		<ul class="nav">	
					<li><a href="#"><img src="images/icon_search.png" alt="Search" title="Search" /></a></li>
					<li><a data-toggle="modal" href="#queueModal">
						<span class="badge badge-success" style="float:right;margin-left:-2px;">2</span>
						<img src="images/icon_runningbuilds.png" alt="Queue &mdash; 2 builds in progress" title="Queue &mdash; 2 builds in progress" />
						</a></li>
					<li class="dropdown"><a href="#" class="dropdown dropdown-toggle" data-toggle="dropdown" style="width:48px; height:48px; margin-top:-8px; background-image:url('images/icon_user.png');">
						<span class="caret"> </span>
						</a>
						<ul class="dropdown-menu pull-right">
                            <li><img src="images/icon_user.png">&nbsp; <strong>John Doe</strong></li>
                            <li class="divider"></li>
                            <li><a href = "#">Settings</a></li>
                            <li><a href = "#">Account details</a></li>
                            <li class="divider"></li>
                            <li><button class="btn btn-primary" href="home.php?page=index">Log Out</button></li>
	                    </ul>
					</li>
				</ul>
			</div>
			<!--Top Right Tools-->
			</div>
		</div>	
			
	</div>
	<!--HEADER-->

     <div id="main" class="container" >
END;
	 
        echo '<!-- Main Content --><!--Top Bar-->';
		echo '<div class="row"><div class="span12" >';
        echo '<h1><span class="glyph enclosed" style="vertical-align:top;">c</span> Projects</h1>';  
        
		//TODO: add name of the project sellected through form requests
		echo '<h2 class="pull-left">'.$projName->setup->name.'</h2>';   
        
		echo '<ul class="nav nav-pills topbarnav pull-left" style="margin-left:0;"> ';      
        echo '<li><a href="home.php?page=projects_myproject">Project</a></li>';       
        echo '<li class="active"><a href="home.php?page=projects_history"><i class="icon-time icon-white"></i> History</a></li>';          
	    echo '<li><a href="home.php?page=projects_settings"><i class="icon-cog icon-blue"></i> Settings</a></li></ul>';
		//echo '<li><a href="home.php?page=projects_settings"><i class="icon-cog icon-blue"></i> Settings</a></li>';
		//echo '<li><a href="home.php?page=projects_permissions"><i class="icon-lock icon-blue"></i> Permissions</a></li></ul>'; 
	    echo '</ul></div></div>'; 
		echo '<!--Top Bar-->';

		echo '<div class="row"><!--MAIN WORK AREA--><div class="span9">';
		echo '<!--Project List--><div class=" tablesattop"><div><table class="table">';
		
		$nr_history = sizeof ($projName->setup->history->item);
		for ($i = 0; $i < $nr_history; $i++)
		{
			echo '<tr><td><a href="#">'.$projName->setup->history->item[$i]->time.'</a></td>';
			echo '<td>'.$projName->setup->history->item[$i]->info.'</strong></td>';
			echo '<td><a href="#"><i class="icon-user"></i> '.$projName->setup->history->item[$i]->person.'</a></td></tr>';
		}
		echo '</table></div></div>';
		
		echo '<div class="row pull-right">';
		echo '<a data-toggle="modal" href="#addHistoryToProject" class="btn btn-large">Add history information</a>';
		echo '</div></div><!--MAIN WORK AREA-->';
		
		echo '<!--SIDEBAR--><div class="span3"><div class="well">';
		echo '<h4>History?</h4>';
		echo '<p>In this section the user that works on the selected project can view the progreses and modification done on the project.</p>';
		echo '<p><a href="https://www.yoctoproject.org/"><strong>More about the Yocto Project &raquo;</strong></a></p></div></div>';
		echo '<!--SIDEBAR--></div></div><!--Main Content-->';
		
 echo <<< END
 
 <!--addHistoryToProject Modal Content-->
<div id="addHistoryToProject" class="modal hide">
     
     <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
       <h3>Add the new group properties</h3>
     </div>
     
     <div class="modal-body">
     
		<form class="form-inline" style="padding-right:20px;">
		<table border="0">
END;
		$today_date = date("d-m-Y");
		$today_time = date("H:i");
		echo '<tr><input type="text" value="'.$today_date.' '.$today_time.'" style="width:190px;"></tr>';
		echo '<tr><input type="text" MAXLENGTH="1500" placeholder="Brief info (1500 characters max)" style="width:190px;"></tr>';
		//TODO: update info in XML file.
echo <<< END
		</table>
        
        </form>
       
     </div>
     
     <div class="modal-footer">
     <a href="#" class="btn" data-dismiss="modal" >Save</a>
     </div>
</div>
<!--addHistoryToProject Modal Content-->
 
<!--Queue Modal Content-->
<div id="queueModal" class="modal hide">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h3>Running Builds</h3>
            </div>
            <div class="modal-body">
              <h4>Build 1</h4>
              <p>Some information about the build.</p>
              
              <div class="progress progress-success">
                    <div class="bar" style="width: 60%;"></div>
              </div>
              
              <div class="alert alert-success">Done! <a href="#">Download Build 1 here.</a></div>

              <h4>Build 2</h4>
              <p>Some information about the build.</p>
              
              <div class="progress progress-striped
     active">
                    <div class="bar " style="width: 60%;"></div>
              </div>
              
              <p><i class="icon-time"></i> 01:23:02 remaining</p>

              <h4>Build 3</h4>
              <p>Some information about the build.</p>

              <div class="progress progress-striped
     active">
                    <div class="bar " style="width: 60%;"></div>
              </div>
              
              <p><i class="icon-time"></i> 01:23:02 remaining</p>

              
            </div>
            <div class="modal-footer">
              <a href="#" class="btn" data-dismiss="modal" >Close</a>
            </div>
          </div>
<!--Queue Modal Content-->

    </div><!-- /container -->


	<footer>
		<div class="container" >
			<div class="row">
				<div class="span3" style="opacity:.65;">
					<p>&copy; 2012 The Yocto Project</p>
				</div>
				<div class="span3">
						<a href="#">About</a>
						<a href="#">Blogs</a>
						<a href="#">Documentation</a>
				</div>
				<div class="span3">
					<a href="#">Privacy Policy</a>
					<a href="#">Terms of Service</a>
					<a href="#">Trademarks</a>
				</div>
				<div class="span3">
					<a href="http://www.linuxfoundation.org" style="height:66px"><img src="images/linux_foundation.png" alt="Linux Foundation"/></a>
				</div>
			</div>
		</div>
	</footer>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap-transition.js"></script>
    <script src="../assets/js/bootstrap-alert.js"></script>
    <script src="../assets/js/bootstrap-modal.js"></script>
    <script src="../assets/js/bootstrap-dropdown.js"></script>
    <script src="../assets/js/bootstrap-scrollspy.js"></script>
    <script src="../assets/js/bootstrap-tab.js"></script>
    <script src="../assets/js/bootstrap-tooltip.js"></script>
    <script src="../assets/js/bootstrap-popover.js"></script>
    <script src="../assets/js/bootstrap-button.js"></script>
    <script src="../assets/js/bootstrap-collapse.js"></script>
    <script src="../assets/js/bootstrap-carousel.js"></script>
    <script src="../assets/js/bootstrap-typeahead.js"></script>


  </body>
</html>

END;

?>
