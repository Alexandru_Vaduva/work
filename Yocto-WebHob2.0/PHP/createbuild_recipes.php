<?php

echo <<< END

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Yocto Web Hob</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
<!--remove and use @import in css in production-->
<link href="../assets/css/bootstrap.css" rel="stylesheet">
<!--remove and use @import in css in production-->
<link href="css/yocto.css" rel="stylesheet">
<link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">
<!--[if !IE 7]>
<style type="text/css">
#wrap {display:table;height:100%}
</style>
<![endif]-->

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="../assets/ico/favicon.ico">
</head>

<body>
<div id="wrap"> 
	
	<!--HEADER-->
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">	
 				<a class="brand" href="home.php?page=index_dashboard">&nbsp;</a>
			
				<ul class="nav" style="text-align:center;">
					<!--SET THE ACTIVE SECTION by adding class="active"-->
					<li class="active"><a href="home.php?page=builds" class="icon-builds">Builds</a></li>
					<li><a href="home.php?page=projects" class="icon-projects">Projects</a></li>
					<!-- <li><a href="home.php?page=groups" class="icon-groups">Groups</a></li> -->
				</ul>
		
			
             <!--Top Right Tools-->
             <div id="top-right-tools">
           		<ul class="nav">	
					<li><a href="#"><img src="images/icon_search.png" alt="Search" title="Search" /></a></li>
					<li><a data-toggle="modal" href="#queueModal">
						<span class="badge badge-success" style="float:right;margin-left:-2px;">2</span>
						<img src="images/icon_runningbuilds.png" alt="Queue &mdash; 2 builds in progress" title="Queue &mdash; 2 builds in progress" />
						</a></li>
					<li class="dropdown"><a href="#" class="dropdown dropdown-toggle" data-toggle="dropdown" style="width:48px; height:48px; margin-top:-8px; background-image:url('images/icon_user.png');">
						<span class="caret"> </span>
						</a>
						<ul class="dropdown-menu pull-right">
                            <li><img src="images/icon_user.png">&nbsp; <strong>John Doe</strong></li>
                            <li class="divider"></li>
                            <li><a href = "#">Settings</a></li>
                            <li><a href = "#">Account details</a></li>
                            <li class="divider"></li>
                            <li><button class="btn btn-primary" href="home.php?page=index">Log Out</button></li>
	                    </ul>
					</li>
				</ul>
			</div>
			<!--Top Right Tools-->
			</div>
		</div>	
			
	</div>
	<!--HEADER-->
	
END;
	/* <!-- Main Content -->  */
	echo '<div id="main" class="container" >';
	
	/* <!--Top Bar--> */
	echo '<div class="row"><div class="span12" ><h1 class="pull-left">Yocto Block Rollout</h1>';
	echo '<ul class="nav nav-pills topbarnav pull-left match-h1" style="margin-left:0;">';
	echo '<li><a href="home.php?page=createbuild_packagegroups">Package Groups</a></li>';
	echo '<li class="active"><a href="home.php?page=createbuild_recipes">Recipes</a></li>';
	echo '<li><a href="home.php?page=createbuild_packages">Packages</a></li>';
	echo '<li><a href="home.php?page=createbuild_buildimage">Build Image</a></li></ul></div></div>';
	/* <!--Top Bar--> */
	
	/* <!--MAIN WORK AREA--> */
	echo '<div class="row"> <div class="span9">';
	echo '<h2><a href="home.php?page=createbuild_packagegroups">Package Groups <i class=" icon-ok"></i></a></h2><hr />';
	echo '<h2>Recipes</h2><form><table class="table table-bordered">';
	echo '<thead><tr><th>&nbsp;</th><th>Task name</th><th>Brought in by</th></tr></thead>';
	
	$build = '../python/show_recipe.py';
	$output = shell_exec($build);
	foreach (preg_split('/],\s*\[/', trim($output, '[]')) as $row) 
	{
        $data = preg_split("/',\s*'/", trim($row, "'"));
        //print_r($data);
	}
	$size = sizeof($data);
	$selected = array();
	for ($i = 0; $i < $size; $i++)
	{
		$data[$i] = str_replace("']", "", $data[$i]);
		array_push($selected, $data[$i]);
		echo '<tr><td class="cb_td_one"><input type="checkbox" id="optionsCheckbox1" value="option1"checked="yes" /></td>';
		echo '<td class="cb_td_two"><label class="checkbox1">'.$data[$i].'</label></td>';
		echo '<td>meta</td></tr>';
	}
		
	echo '</table><a href="home.php?page=createbuild_packages" type="submit" class="btn btn-primary btn-large"><i class="icon-fire icon-white"></i> Build Packages</a>';
	/* <!--Packages--> */
	echo '</form><hr /><h2><a href="home.php?page=createbuild_packages">Packages</a></h2>';
	echo '<hr /><h2><a href="home.php?page=createbuild_buildimage">Build Image</a></h2>';
	/* <!--Packages--> */
	
	/* <!--MAIN WORK AREA--> */
	echo '</div><div class="span3"><div class="well">';
	
	/* <!--SIDEBAR--> */
	echo '<h4>What are recipes?</h4>';
	echo '<p>Recipes are a set of instructions for building packages. A recipe describes where you get source code and which patches to apply. Recipes describe dependencies for libraries or for other recipes, and they also contain configuration and compilation options. Recipes contain the logical unit of execution, the software/images to build, and use the .bb file extension.</p>';
	echo '<p>Packages are the output from a baked recipe. A package is generally the compiled binaries produced from the recipe`s sources. You <i>bake</i> something by running it through BitBake.</p>';
	echo '<div class="thumbnail"> <img src="images/embeddedlinux.jpg" alt=""> </div>';
	echo '<p><a href="http://www.yoctoproject.org/docs/1.4/dev-manual/dev-manual.html#usingpoky-extend-customimage-customtasks">More about the build process and how to reduce errors in your builds &#187;</a></p>'; 
	echo '</div></div></div></div>';
	
echo <<< END

<!--Main Content--> 

<!--Queue Modal Content-->
<div id="queueModal" class="modal hide">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h3>Running Builds</h3>
	</div>
	<div class="modal-body">
		<h4>Build 1</h4>
		<p>Some information about the build.</p>
		<div class="progress progress-success">
			<div class="bar" style="width: 60%;"></div>
		</div>
		<div class="alert alert-success">Done! <a href="#">Download Build 1 here.</a></div>
		<h4>Build 2</h4>
		<p>Some information about the build.</p>
		<div class="progress progress-striped
     active">
			<div class="bar " style="width: 60%;"></div>
		</div>
		<p><i class="icon-time"></i> 01:23:02 remaining</p>
		<h4>Build 3</h4>
		<p>Some information about the build.</p>
		<div class="progress progress-striped
     active">
			<div class="bar " style="width: 60%;"></div>
		</div>
		<p><i class="icon-time"></i> 01:23:02 remaining</p>
	</div>
	<div class="modal-footer"> <a href="#" class="btn" data-dismiss="modal" >Close</a> </div>
</div>
<!--Queue Modal Content-->
</div>
<!-- /container -->

<footer>
	<div class="container" >
		<div class="row">
			<div class="span3" style="opacity:.65;">
				<p>&copy; 2012 The Yocto Project</p>
			</div>
			<div class="span3">
					<a href="#">About</a>
					<a href="#">Blogs</a>
					<a href="#">Documentation</a>
			</div>
			<div class="span3">
				<a href="#">Privacy Policy</a>
				<a href="#">Terms of Service</a>
				<a href="#">Trademarks</a>
			</div>
			<div class="span3">
				<a href="http://www.linuxfoundation.org" style="height:66px"><img src="images/linux_foundation.png" alt="Linux Foundation"/></a>
			</div>
		</div>
	</div>
</footer>

<!-- Le javascript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="../assets/js/jquery.js"></script> 
<script src="../assets/js/bootstrap-transition.js"></script> 
<script src="../assets/js/bootstrap-alert.js"></script> 
<script src="../assets/js/bootstrap-modal.js"></script> 
<script src="../assets/js/bootstrap-dropdown.js"></script> 
<script src="../assets/js/bootstrap-scrollspy.js"></script> 
<script src="../assets/js/bootstrap-tab.js"></script> 
<script src="../assets/js/bootstrap-tooltip.js"></script> 
<script src="../assets/js/bootstrap-popover.js"></script> 
<script src="../assets/js/bootstrap-button.js"></script> 
<script src="../assets/js/bootstrap-collapse.js"></script> 
<script src="../assets/js/bootstrap-carousel.js"></script> 
<script src="../assets/js/bootstrap-typeahead.js"></script>
</body>
</html>

END;

?>
