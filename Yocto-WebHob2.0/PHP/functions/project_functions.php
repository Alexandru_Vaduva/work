<?php

#error_reporting(E_ALL); 
#ini_set('display_errors','1'); 

if (!defined('sessions_path')) define ('sessions_path', './sessions.txt');
if (!defined('projects_path')) define ('projects_path', '../../XML/projects.xml');

//------------------------------------------------------------------------------
// sessions related functionality
//------------------------------------------------------------------------------
//retrieve sessions array, save array and return number of elements
function getNumSessions() 
{
    $sessionsArray = unserialize(file_get_contents(sessions_path));
    $numSessions = 0;
    if (is_array($sessionsArray)) 
    {
        $numSessions = count($sessionsArray) / 2;
    }
    return $numSessions;
}

//add session
function addSession($session_id) 
{
    $sessionsArray = unserialize(file_get_contents(sessions_path));
    if (!is_array($sessionsArray)) 
    {
        $sessionsArray = array($session_id, time());
    } 
    else 
    {
        array_push($sessionsArray, $session_id, time());
    }
    $fp = fopen(sessions_path, 'w+') or die("I could not open sessions.txt");
    fwrite($fp, serialize($sessionsArray));
    fclose($fp);
}

//update session
function updateSession($session_id) 
{
    $sessionsArray = unserialize(file_get_contents(sessions_path));
    $numSessions = count($sessionsArray) / 2;
    for ($i = 0; $i < $numSessions; $i++) 
    {
        if ($sessionsArray[$i * 2] == $session_id) 
        {
            $sessionsArray[$i * 2 + 1] = time();
            break;
        }
    }
    $fp = fopen(sessions_path, 'w+') or die("I could not open sessions.txt");
    fwrite($fp, serialize($sessionsArray));
    fclose($fp);
}

//remove session
function removeSession($session_id) 
{
    $sessionsArray = unserialize(file_get_contents(sessions_path));
    $numSessions = count($sessionsArray) / 2;
    for ($i = 0; $i < $numSessions; $i++) 
    {
        if ($sessionsArray[$i * 2] == $session_id) 
        {
            array_splice($sessionsArray, $i * 2, 2);
            break;
        }
    }
    $fp = fopen(sessions_path, 'w+') or die("I could not open sessions.txt");
    fwrite($fp, serialize($sessionsArray));
    fclose($fp);
}


//------------------------------------------------------------------------------
//login related functionality
//------------------------------------------------------------------------------
//login
function doLogin($passwd, $email) 
{
    global $users;

    for ($i = 0; $i < sizeof($users->list->user); $i++) 
    {
        if (strcmp($users->list->user[$i]->password, $passwd) == 0 &&
			strcmp($users->list->user[$i]->email, $email) == 0)
        {
            if ((int) $users->list->user[$i]->enabled == 1) 
            {
                $_SESSION['loggedin'] = 1;
            }
            echo "User gasit si logat.<br>";
            break;
        }
    }
}

//det actual date time
function get_Datetime_Now() 
{
    $tz_object = new DateTimeZone('EUROPE');
    $datetime = new DateTime();
    $datetime->setTimezone($tz_object);     
	
	return $datetime->format('Y\-m\-d\ h:i:s');
}




//------------------------------------------------------------------------------
//users related functionality
//------------------------------------------------------------------------------
//check if user still exists in configuration or still enabled
function checkUserEnabled() 
{
    global $users;

    if ($_SESSION['loggedin']) 
    {
        for ($i = 0; $i < sizeof($users->list->user); $i++) 
        {
            if ((int) $users->list->user[$i]->enabled == 1) 
            {
                return true;
            }
        }
        return false;
    } 
    else 
    {
        return true;
    }
}


//------------------------------------------------------------------------------
// projects tab related functionality
//------------------------------------------------------------------------------
//get project group members number
function getProjectMemberNum($prj, $id)
{
	global $projects;
	global $groups;

	$index = 0;
	for ($i = 0; $i < sizeof($groups->list->item); $i++) 
	{		
		if (strcmp($projects->list->item[$prj]->groups->name[$id], $groups->list->item[$i]->name) == 0) 
		{
			$index = sizeof($groups->list->item[$i]->user);
			break;
		}
	}
	
	return $index;
}

//get project group user name
function getProjectMemberName($prj, $id, $idx)
{
	global $projects;
	global $groups;
	
	$name = "";
	for ($i = 0; $i < sizeof($groups->list->item); $i++) 
	{		
		if (strcmp($projects->list->item[$prj]->groups->name[$id], $groups->list->item[$i]->name) == 0)
		{
			$name = $groups->list->item[$i]->user[$idx]->username;
			break;
		}
	}
	
	return $name;
}

//get project group picture
function getProjectMemberPicture($prj, $id, $idx)
{
	global $projects;
	global $groups;
	
	$picture = "images/user.png";
	for ($i = 0; $i < sizeof($groups->list->item); $i++) 
	{		
		if (strcmp($projects->list->item[$prj]->groups->name[$id], $groups->list->item[$i]->name) == 0) 
		{
			if (!empty($groups->list->item[$i]->user[$idx]->picture) && strcmp($groups->list->item[$i]->user[$idx]->picture, " ") != 0)
			{
				$picture = $groups->list->item[$i]->user[$idx]->picture;
				break;
			}
		}
	}
	
	return $picture;
}

function getPictureByName($group, $name)
{
	global $groups;
	
	$picture = "images/user.png";
	for ($i = 0; $i < sizeof($groups->list->item); $i++) 
	{		
		if (strcmp($group, $groups->list->item[$i]->name) == 0) 
		{
			for ($j = 0; $j < sizeof($groups->list->item->user); $j++)
			{
				if (strcmp($name, $groups->list->item[$i]->user[$j]->username) == 0)
					if (!empty($groups->list->item[$i]->user[$j]->picture) && strcmp($groups->list->item[$i]->user[$j]->picture, " ") != 0)
					{
						$picture = $groups->list->item[$i]->user[$j]->picture;
						break;
					}
			}
		}
	}
	
	return $picture;
}

?>
