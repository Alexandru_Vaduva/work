<?php
/*
error_reporting(E_ALL); 
ini_set('display_errors','1'); 

include "test.php";

global $projName;
*/
echo <<< END

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Yocto Web Hob</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
<!--remove and use @import in css in production-->
<link href="../assets/css/bootstrap.css" rel="stylesheet">
<!--remove and use @import in css in production-->
<link href="css/yocto.css" rel="stylesheet">
<link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">
<!--[if !IE 7]>
<style type="text/css">
#wrap {display:table;height:100%}
</style>
<![endif]-->

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="../assets/ico/favicon.png">

</head>

<body>

<div id="wrap">

	<!--HEADER-->
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">	
 				<a class="brand" href="home.php?page=index_dashboard">&nbsp;</a>
			
				<ul class="nav" style="text-align:center;">
					<!--SET THE ACTIVE SECTION by adding class="active"-->
					<li><a href="home.php?page=builds" class="icon-builds">Builds</a></li>
					<li class="active"><a href="home.php?page=projects" class="icon-projects">Projects</a></li>
					<!-- <li><a href="home.php?page=groups" class="icon-groups">Groups</a></li> -->
				</ul>
		
			
             <!--Top Right Tools-->
             <div id="top-right-tools">
           		<ul class="nav">	
					<li style="visibility:hidden;"><a href="#"><img src="images/icon_search.png" alt="Search" title="Search" /></a></li>
					<li><a data-toggle="modal" href="#queueModal">
						<span class="badge badge-success" style="float:right;margin-left:-2px;">2</span>
						<img src="images/icon_runningbuilds.png" alt="Queue &mdash; 2 builds in progress" title="Queue &mdash; 2 builds in progress" />
						</a></li>
					<li class="dropdown"><a href="#" class="dropdown dropdown-toggle" data-toggle="dropdown" style="width:48px; height:48px; margin-top:-8px; background-image:url('images/icon_user.png');">
						<span class="caret"> </span>
						</a>
						<ul class="dropdown-menu pull-right">
                            <li><img src="images/icon_user.png">&nbsp; <strong>John Doe</strong></li>
                            <li class="divider"></li>
                            <li><a href = "#">Settings</a></li>
                            <li><a href = "#">Account details</a></li>
                            <li class="divider"></li>
                            <li><button class="btn btn-primary" href="home.php?page=index">Log Out</button></li>
	                    </ul>
					</li>
				</ul>
			</div>
			<!--Top Right Tools-->
			</div>
		</div>	
			
	</div>
	<!--HEADER-->
	
	<div id="main" class="container" >

          <!-- Main Content -->
		  <!--Top Bar-->
			<div class="row">
				<div class="span12" >
					
			         <h1><span class="glyph enclosed" style="vertical-align:top;">c</span> Projects</h1>

END;
	
	//TODO: add name of the project sellected through form requests
    echo '<h2 class="pull-left">'.$projName->setup->name.'</h2>';
	
	echo '<ul class="nav nav-pills topbarnav pull-left" style="margin-left:0;">';
	echo '<li><a href="home.php?page=projects_myproject">Project</a></li>';
	echo '<li><a href="home.php?page=projects_history"><i class="icon-time icon-blue"></i> History</a></li>';
	echo '<li><a href="home.php?page=projects_settings"><i class="icon-cog icon-blue"></i> Settings</a></li>';
	echo '<li class="active"><a href="home.php?page=projects_permissions"><i class="icon-lock icon-white"></i> Permissions</a></li>';
	echo '</ul></div></div><!--Top Bar--><div class="row"><!--MAIN WORK AREA--><div class="span9">';
	
	$nr_groups = sizeof ($projName->setup->groups->item);
	for ($i = 0; $i < $nr_groups; $i++)
	{
		echo '<!--Dev Group--><div class="row grouppods">';
		echo '<div class="span2"><h3>'.$projName->setup->groups->item[$i]->name.'</h3></div>';
		echo '<div class="span7"><h4>Group Permissions</h4><p>Group members inherit group permissions by default.</p>';
		
		echo '<form class="horizontal-form"><fieldset><div class="control-group ">';
		echo '<div class="controls"><label class="checkbox inline">';
		$edit_project = " ";
		if ($projName->setup->groups->item[$i]->edit == 1)
			$edit_project = " checked";
		echo '<input type="checkbox" id="inlineCheckbox1" value="createproject"'.$edit_project.'>Edit Project</label>';
		$access_builds = " ";
		if ($projName->setup->groups->item[$i]->access_delete == 1)
			$access_builds = " checked";
		echo '<label class="checkbox inline"><input type="checkbox" id="inlineCheckbox2" value="access_deletebuilds"'.$access_builds.'> Access/delete builds</label>';
		$modify_recipes = " ";
		if ($projName->setup->groups->item[$i]->modify == 1)
			$modify_recipes = " checked";
		echo '<label class="checkbox inline"><input type="checkbox" id="inlineCheckbox3" value="addlayers"'.$modify_recipes.'>Modify Recipes</label>';
		echo '</div></div></fieldset></form>';
		
		echo '<h4>Group Members</h4><!--Group Members--><div class="row">';
		
		$nr_members = sizeof ($projName->setup->groups->item[$i]->members->item);
		for ($j = 0; $j < $nr_members; $j++)
		{
			echo '<div class="span3"><img src='.getPictureByName($projName->setup->name, $projName->setup->groups->item[$i]->name).' class="pull-left" alt="" style="width:36px; height:36px; margin-right:5px;" />';
			echo '<p>'.$projName->setup->groups->item[$i]->members->item->name.' <a data-toggle="modal" href="#userpermissionsModal"><i class="icon-cog"></i></a><br />';
			echo '<a href="#"><i class="icon-envelope"></i> '.$projName->setup->groups->item[$i]->members->item->email.'</a></p></div>';
			
			//<!--User Permissions Modal Content-->
			echo '<div id="userpermissionsModal" class="modal hide"><div class="modal-header">';
			echo '<button type="button" class="close" data-dismiss="modal">&times;</button>';
			echo '<h3>User level permissions</h3></div><div class="modal-body">';
			$create_project = " ";
			if ($projName->setup->groups->item[$i]->edit == 1)
				$create_project = " checked";
			echo '<label class="checkbox"><input type="checkbox"'.$create_project.'> Create Project</label>';
			$access_delete = " ";
			if ($projName->setup->groups->item[$i]->edit == 1)
				$access_delete = " checked";
			echo '<label class="checkbox"><input type="checkbox"'.$access_delete.'> Access/delete builds </label>';
			$add_layers = " ";
			if ($projName->setup->groups->item[$i]->edit == 1)
				$add_layers = " checked";
			echo '<label class="checkbox"><input type="checkbox"'.$add_layers.'> Add external layers </label>';
			echo '</div><div class="modal-footer"><a href="#" class="btn" data-dismiss="modal" >Close</a></div></div>';
			//<!--User Permissions Modal Content-->
		}
		echo '</div><!--Group Members--><button class="btn"><i class="icon-plus-sign"></i> Add someone to the group</button></div>';
		
		echo '</div><!--Dev Group-->';
	}
	echo '</div><!--MAIN WORK AREA--><!--SIDEBAR--><div class="span3"><div class="well"><h4>Settings and Projects</h4>';
	echo '<p>This section is similar with the <b>Groups</b> section, only that here is presented only the project specific information.</p>';
	echo '<p><a href="http://www.yoctoproject.org/docs/current/yocto-project-qs/yocto-project-qs.html"><strong>More about build properties &raquo;</strong></a></p></div></div>';
	echo '<!--SIDEBAR--></div></div><!--Main Content-->';
	
 echo <<< END
		  	
<!--Queue Modal Content-->
<div id="queueModal" class="modal hide">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h3>Running Builds</h3>
            </div>
            <div class="modal-body">
              <h4>Build 1</h4>
              <p>Some information about the build.</p>
              
              <div class="progress progress-success">
                    <div class="bar" style="width: 60%;"></div>
              </div>
              
              <div class="alert alert-success">Done! <a href="#">Download Build 1 here.</a></div>

              <h4>Build 2</h4>
              <p>Some information about the build.</p>
              
              <div class="progress progress-striped
     active">
                    <div class="bar " style="width: 60%;"></div>
              </div>
              
              <p><i class="icon-time"></i> 01:23:02 remaining</p>

              <h4>Build 3</h4>
              <p>Some information about the build.</p>

              <div class="progress progress-striped
     active">
                    <div class="bar " style="width: 60%;"></div>
              </div>
              
              <p><i class="icon-time"></i> 01:23:02 remaining</p>

              
            </div>
            <div class="modal-footer">
              <a href="#" class="btn" data-dismiss="modal" >Close</a>
            </div>
          </div>
<!--Queue Modal Content-->

<!--Add Person Modal Content-->
<div id="addpersonModal" class="modal hide">
     
     <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
       <h3>Add someone to your project</h3>
     </div>
     
     <div class="modal-body">
	
     <p>Adding someone to your project is easy. They don't have to be a member of your group. Just put in their email, type a message if you like and they'll be sent a message and have an ID automatically created for them.</p>
	
     <form class="form">
     <input type="text" class="input-small" placeholder="Email Address">
          <div class="controls">
	<textarea class="input-textarea_modal" id="textarea02" rows="3"></textarea>
	</div>
     <button type="submit" class="btn">Clear</button><br />
     
     </div>
     <div class="modal-footer">
	
     <a href="#" class="btn" data-dismiss="modal" >Cancel</a>
     <button class="btn btn-primary"><i class="icon-plus-sign icon-white"></i> Add</button>
     </form>
     
     </div>
     
</div>
<!--Add Person Modal Content-->

<!--Build Schedule Modal Content-->
<div id="buildscheduleModal" class="modal hide">
     
     <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
       <h3>Schedule your build</h3>
     </div>
     
     <div class="modal-body">
	
	<div class="alert alert-info">
	<strong>Hey!</strong> Be sure that everything is set correctly before you schedule to avoide wasting resources.
	</div>
	
     <h4>Image to build</h4>
         
         <div class="well">
	<p><strong>Machine:</strong> beagleboard</p>
	<p><strong>Base image:</strong> core-image-minimal-dev</p>
         </div>
         
     <button class="btn-primary btn-large">Add to Queue now</button>
     
     <hr />
     
     <h4>Schedule it for later</h4>
     <div class="alert alert-error">Devs, please note that a datepicker that plays nicely with modal windows is being looked into</div>
     <form class="well form-inline">
     <label>Date</label>
     <input type="text" class="input-small" placeholder="5/25/12">
     <label>Time</label>
     <input type="password" class="input-small" placeholder="12:00">
     <button type="submit" class="btn btn-primary">Add to Queue</button>
     </form>
     </div>
	
     <div class="modal-footer">
     <a href="#" class="btn" data-dismiss="modal" >Cancel</a>
     </div>
     
</div>
<!--Build Schedule Modal Content-->

    </div><!-- /container -->


	<footer>
		<div class="container" >
			<div class="row">
				<div class="span3" style="opacity:.65;">
					<p>&copy; 2012 The Yocto Project</p>
				</div>
				<div class="span3">
						<a href="#">About</a>
						<a href="#">Blogs</a>
						<a href="#">Documentation</a>
				</div>
				<div class="span3">
					<a href="#">Privacy Policy</a>
					<a href="#">Terms of Service</a>
					<a href="#">Trademarks</a>
				</div>
				<div class="span3">
					<a href="http://www.linuxfoundation.org" style="height:66px"><img src="images/linux_foundation.png" alt="Linux Foundation"/></a>
				</div>
			</div>
		</div>
	</footer>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap-transition.js"></script>
    <script src="../assets/js/bootstrap-alert.js"></script>
    <script src="../assets/js/bootstrap-modal.js"></script>
    <script src="../assets/js/bootstrap-dropdown.js"></script>
    <script src="../assets/js/bootstrap-scrollspy.js"></script>
    <script src="../assets/js/bootstrap-tab.js"></script>
    <script src="../assets/js/bootstrap-tooltip.js"></script>
    <script src="../assets/js/bootstrap-popover.js"></script>
    <script src="../assets/js/bootstrap-button.js"></script>
    <script src="../assets/js/bootstrap-collapse.js"></script>
    <script src="../assets/js/bootstrap-carousel.js"></script>
    <script src="../assets/js/bootstrap-typeahead.js"></script>


  </body>
</html>

END;

?>
