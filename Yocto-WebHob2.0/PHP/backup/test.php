<?php

if (!defined('projects_path')) define ('projects_path', '../XML/projects.xml');
if (!defined('users_path')) define ('users_path', '../XML/users.xml');
if (!defined('groups_path')) define ('groups_path', '../XML/groups.xml');
if (!defined('templates_path')) define ('templates_path', '../XML/templates.xml');

session_start();
//phpinfo();
//TODO: remove this for production
error_reporting(E_ALL);
ini_set('display_errors','1'); 

// for logging use:
#ini_set('log_errors',1);
#ini_set('display_errors',0);
#error_reporting(E_ALL);

//for php > 5.2 better use this example to get error reason:
#ini_set('track_errors', 1);
#$fh = fopen('lalala', 'r');
#if ( !$fh ) {
#  echo 'fopen failed. reason: ', $php_errormsg;
#}

include "functions/project_functions.php";


//------------------------------------------------------------------------------
// Load and update XML related info
//------------------------------------------------------------------------------
$projects = simplexml_load_file(projects_path);
$users = simplexml_load_file(users_path);
$groups = simplexml_load_file(groups_path);
$templates = simplexml_load_file(templates_path);

/* The project_name will be sent through a request form to the info page */
if (!defined('project_name')) define ('project_name', '../XML/YoctoBlockRollout.xml');
$projName = simplexml_load_file(project_name);
if (!defined('template_name')) define ('template_name', '../XML/Default Template.xml');
$templateName = simplexml_load_file(template_name);

//TODO: cleanup
/*echo "<br>-----Before page-----<br>Session ID: ".session_id();
echo "<br>Session data: ";
print_r($_SESSION);
echo "<br>Request data: ";
print_r($_REQUEST);*/

				/* Projects */
if (!isset($_SESSION['projects_info']) OR ((int)$_SESSION['projects_iteration'] != (int)$projects->iteration))
{
   $_SESSION['projects_info'] = $projects->asXML();
   $_SESSION['projects_iteration'] = (int)$projects->iteration;
}
else
{
	$projects = simplexml_load_string($_SESSION['projects_info']);
}

				/* Users */
if (!isset($_SESSION['users_info']) OR ((int)$_SESSION['users_iteration'] != (int)$users->iteration))
{
   $_SESSION['users_info'] = $users->asXML();
   $_SESSION['users_iteration'] = (int)$users->iteration;
}
else
{
	$users = simplexml_load_string($_SESSION['users_info']);
}

				/* Groups */
if (!isset($_SESSION['groups_info']) OR ((int)$_SESSION['groups_iteration'] != (int)$groups->iteration))
{
   $_SESSION['groups_info'] = $groups->asXML();
   $_SESSION['groups_iteration'] = (int)$groups->iteration;
}
else
{
	$groups = simplexml_load_string($_SESSION['groups_info']);
}

				/* Project File Name */
if (!isset($_SESSION['projName_info']) OR ((int)$_SESSION['projName_iteration'] != (int)$projName->iteration))
{
   $_SESSION['projName_info'] = $projName->asXML();
   $_SESSION['projName_iteration'] = (int)$projName->iteration;
}
else
{
	$projName = simplexml_load_string($_SESSION['projName_info']);
}

				/* Templates */
if (!isset($_SESSION['templates_info']) OR ((int)$_SESSION['templates_iteration'] != (int)$templates->iteration))
{
   $_SESSION['templates_info'] = $templates->asXML();
   $_SESSION['templates_iteration'] = (int)$templates->iteration;
}
else
{
	$templates = simplexml_load_string($_SESSION['templates_info']);
}

				/* Template File Name */
if (!isset($_SESSION['templateName_info']) OR ((int)$_SESSION['templateName_iteration'] != (int)$templateName->iteration))
{
   $_SESSION['templateName_info'] = $templateName->asXML();
   $_SESSION['templateName_iteration'] = (int)$templateName->iteration;
}
else
{
	$templateName = simplexml_load_string($_SESSION['templateName_info']);
}

//echo sizeof($projects->list->item)." - ";
//echo sizeof($users->list->user)." - ";
//echo sizeof($groups->list->item)." ";
//print_r($templateName);

//------------------------------------------------------------------------------
// Session related info
//------------------------------------------------------------------------------
//existing session, check if logout requested
if (isset($_SESSION['started']))
{
	 //check if logout requested or user disabled/deleted
    if (!checkUserEnabled()) 
    {
        removeSession(session_id());
        unset($_SESSION['started']);
        $page = "builds";
    }
    else 
    {
        $_SESSION['started'] = time();
        updateSession(session_id());
    }
}

//new session, either new connection, after logout
if (!isset($_SESSION['started']))
{
    $numSessions = getNumSessions();
    //echo $numSessions;
    if ($numSessions < getMaxSessions()) 
    {
        $_SESSION['started'] = time();
        $_SESSION['loggedin'] = 0;
        //TODO: add the main pages that require configuration changes
        //$_SESSION['builds'] = 0;
        //$_SESSION['groups'] = 0;
        //$_SESSION['projects'] = 0;        
        addSession(session_id());
    } 
    else 
    {
        session_destroy();
        echo "Maximum number of contemporary session reached, please try again later";
        die();
    }
}

?>
