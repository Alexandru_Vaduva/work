<?php

/*
error_reporting(E_ALL); 
ini_set('display_errors','1'); 

include "test.php";

global $projects;
global $projName;
*/
echo <<< END

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Yocto Web Hob</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
 <!--remove and use @import in css in production-->
 <link href="../assets/css/bootstrap.css" rel="stylesheet">
 <!--remove and use @import in css in production-->
<link href="css/yocto.css" rel="stylesheet">
<link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">
  <!--[if !IE 7]>
      <style type="text/css">
                #wrap {display:table;height:100%}
      </style>
<![endif]-->
 

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="../assets/ico/favicon.ico">

</head>

<body>

<div id="wrap">

	<!--HEADER-->
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">	
 				<a class="brand" href="home.php?page=index_dashboard">&nbsp;</a>
			
				<ul class="nav" style="text-align:center;">
					<!--SET THE ACTIVE SECTION by adding class="active"-->
					<li><a href="home.php?page=builds" class="icon-builds" value = >Builds</a></li>
					<li><a href="home.php?page=projects" class="icon-projects">Projects</a></li>
					<li><a href="home.php?page=groups" class="icon-groups">Groups</a></li>
				</ul>
		
			
             <!--Top Right Tools-->
             <div id="top-right-tools">
           		<ul class="nav">	
					<li style="visibility:hidden;"><a href="#"><img src="images/icon_search.png" alt="Search" title="Search" /></a></li>
					<li><a data-toggle="modal" href="#queueModal">
						<span class="badge badge-success" style="float:right;margin-left:-2px;">2</span>
						<img src="images/icon_runningbuilds.png" alt="Queue &mdash; 2 builds in progress" title="Queue &mdash; 2 builds in progress" />
						</a></li>
					<li class="dropdown"><a href="#" class="dropdown dropdown-toggle" data-toggle="dropdown" style="width:48px; height:48px; margin-top:-8px; background-image:url('images/icon_user.png');">
						<span class="caret"> </span>
						</a>
						<ul class="dropdown-menu pull-right">
                            <li><img src="images/icon_user.png">&nbsp; <strong>John Doe</strong></li>
                            <li class="divider"></li>
                            <li><a href = "#">Settings</a></li>
                            <li><a href = "#">Account details</a></li>
                            <li class="divider"></li>
                            <li><button class="btn btn-primary" href="home.php?page=index">Log Out</button></li>
	                    </ul>
					</li>
				</ul>
			</div>
			<!--Top Right Tools-->
			</div>
		</div>	
			
	</div>
	<!--HEADER-->

END;
	/* <!-- Main Content --> */
	echo '<div id="main" class="container" ><div class="row main"><div class="span12"><div class="row">';
	echo '<div class="span3"><h1><span class="glyph enclosed" style="vertical-align:top;">o</span> Groups</h1></div>';
	//echo '<div class="span6"><p class="match-h1">First thing you would read is probably some introductory text about groups in Yocto and what they do really quickly. This is different from the stuff in the side bar.</p></div>';
	echo '</div></div></div><div class="row"><div class="span9">';
	
	/* <!--MAIN WORK AREA--> */
	$nr_projects = sizeof($projects->list->item);
	for ($i = 0; $i < $nr_projects; $i++)
	{
		$nr_groups = sizeof($projects->list->item[$i]->groups->name);
		for ($j = 0; $j < $nr_groups; $j++) 
		{
			echo '<div class="row grouppods"><div class="span2"><h3>'.$projects->list->item[$i]->groups->name[$j].'</h3>';
			echo '<br><h4>'.$projects->list->item[$i]->name.'</h4> </div>';
						 
			echo '<div class="span7"><h4>Group Permissions</h4><p>Group members inherit group permissions by default.</p>';
			echo '<form class="horizontal-form"><fieldset><div class="control-group "><div class="controls">';
			echo '<label class="checkbox inline"><input type="checkbox" id="inlineCheckbox1" value="createproject">Edit Project</label>';
			echo '<label class="checkbox inline"><input type="checkbox" id="inlineCheckbox2" value="access_deletebuilds"> Access/delete builds</label>';
			echo '<label class="checkbox inline"><input type="checkbox" id="inlineCheckbox3" value="addlayers">Modify Recipes</label>';
			echo '</div></div></fieldset></form>';
			
			$loc = '../XML/'.$projects->list->item[$i]->name.'.xml';
			$proj = simplexml_load_file($loc);
			$proj = simplexml_load_string($proj->asXML());
			
			if (strcmp($projects->list->item[$i]->name, $proj->setup->name) == 0)
			{
				echo '<h4>Group Members</h4><div class="row">';
				$nr_members = sizeof ($proj->setup->groups->item[$j]->members->item);
				for ($k = 0; $k < $nr_members; $k++)
				{
					echo ' <div class="span3"><img src='.getPictureByName($proj->setup->name, $proj->setup->groups->item[$j]->name).' class="pull-left" alt="" style="width:36px; height:36px; margin-right:5px;" />';
					echo '<p>'.$proj->setup->groups->item[$j]->members->item->name.' <a data-toggle="modal" href="#userpermissionsModal"><i class="icon-cog"></i></a><br />';
					echo '<a href="#"><i class="icon-envelope"></i> '.$proj->setup->groups->item[$j]->members->item->email.'</a></p></div>';
				}
				
				echo '<div id="userpermissionsModal" class="modal hide"><div class="modal-header">';
				echo '<button type="button" class="close" data-dismiss="modal">&times;</button>';
				echo '<h3>User level permissions</h3></div><div class="modal-body">';
				$create_project = " ";
				if ($proj->setup->groups->item[$j]->edit == 1)
					$create_project = " checked";
				echo '<label class="checkbox"><input type="checkbox"'.$create_project.'> Create Project</label>';
				$access_delete = " ";
				if ($proj->setup->groups->item[$j]->edit == 1)
					$access_delete = " checked";
				echo '<label class="checkbox"><input type="checkbox"'.$access_delete.'> Access/delete builds </label>';
				$add_layers = " ";
				if ($proj->setup->groups->item[$j]->edit == 1)
					$add_layers = " checked";
				echo '<label class="checkbox"><input type="checkbox"'.$add_layers.'> Add external layers </label>';
				echo '</div><div class="modal-footer"><a href="#" class="btn" data-dismiss="modal" >Close</a></div></div>';
				
				echo '</div><button class="btn"><i class="icon-plus-sign"></i> Add someone to the group</button></div></div>';
			}
			else
				echo '<button class="btn"><i class="icon-plus-sign"></i> Add someone to the group</button></div></div>';
		}
	}
	
	echo '<div class="row pull-right">';
	echo '<a data-toggle="modal" href="#addAGroupToProject" class="btn btn-large">Add a group</a>';
	echo '</div>';
	
	echo '</div><div class="span3"><div class="well"><h4>How do I use Groups with Yocto?</h4>';
	echo '<p>The <b>Groups</b> section is used for presenting with the possibility to work in a team for a given project.</p>';
	echo '<p>This section also offers control over the access and permissions for every member and groups from the user circle. There is also the possibility to keep all the members in contact.</p>';
	echo '</div></div></div></div>';
	
echo <<< END

<!--addAGroupToProject Modal Content-->
<div id="addAGroupToProject" class="modal hide">
     
     <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
       <h3>Add the new group properties</h3>
     </div>
     
     <div class="modal-body">
     
		<form class="form-inline" style="padding-right:20px;">
		<table border="0">
		<tr><input type="text" placeholder="Group name" style="width:190px;"></tr>
		<tr><input type="text" placeholder="Project name" style="width:190px;"></tr>
		</table>
       
        <form class="form-inline form-permissions" style="padding-right:20px;">      
        <label class="checkbox">
        <input type="checkbox"> Edit Project
        </label>
        
        <form class="form-inline" style="padding-right:20px;">
        <label class="checkbox">
        <input type="checkbox"> Access/delete builds
        </label>
        
        <form class="form-inline" style="padding-right:20px;">
        <label class="checkbox">
        <input type="checkbox"> Modify Recipes
        </label>
        
        </form>
       
     </div>
     
     <div class="modal-footer">
     <a href="#" class="btn" data-dismiss="modal" >Save</a>
     </div>
</div>
<!--addAGroupToProject Modal Content-->

<!--Queue Modal Content-->
<div id="queueModal" class="modal hide">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h3>Running Builds</h3>
            </div>
            <div class="modal-body">
              <h4>Build 1</h4>
              <p>Some information about the build.</p>
              
              <div class="progress progress-success">
                    <div class="bar" style="width: 60%;"></div>
              </div>
              
              <div class="alert alert-success">Done! <a href="#">Download Build 1 here.</a></div>

              <h4>Build 2</h4>
              <p>Some information about the build.</p>
              
              <div class="progress progress-striped
     active">
                    <div class="bar " style="width: 60%;"></div>
              </div>
              
              <p><i class="icon-time"></i> 01:23:02 remaining</p>

              <h4>Build 3</h4>
              <p>Some information about the build.</p>

              <div class="progress progress-striped
     active">
                    <div class="bar " style="width: 60%;"></div>
              </div>
              
              <p><i class="icon-time"></i> 01:23:02 remaining</p>

              
            </div>
            <div class="modal-footer">
              <a href="#" class="btn" data-dismiss="modal" >Close</a>
            </div>
          </div>
<!--Queue Modal Content-->

    </div> <!-- /container -->
</div>

<footer>
	<div class="container" >
		<div class="row">
			<div class="span3" style="opacity:.65;">
				<p>&copy; 2012 The Yocto Project</p>
			</div>
			<div class="span3">
					<a href="#">About</a>
					<a href="#">Blogs</a>
					<a href="#">Documentation</a>
			</div>
			<div class="span3">
				<a href="#">Privacy Policy</a>
				<a href="#">Terms of Service</a>
				<a href="#">Trademarks</a>
			</div>
			<div class="span3">
				<a href="http://www.linuxfoundation.org" style="height:66px"><img src="images/linux_foundation.png" alt="Linux Foundation"/></a>
			</div>
		</div>
	</div>
</footer>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap-transition.js"></script>
    <script src="../assets/js/bootstrap-alert.js"></script>
    <script src="../assets/js/bootstrap-modal.js"></script>
    <script src="../assets/js/bootstrap-dropdown.js"></script>
    <script src="../assets/js/bootstrap-scrollspy.js"></script>
    <script src="../assets/js/bootstrap-tab.js"></script>
    <script src="../assets/js/bootstrap-tooltip.js"></script>
    <script src="../assets/js/bootstrap-popover.js"></script>
    <script src="../assets/js/bootstrap-button.js"></script>
    <script src="../assets/js/bootstrap-collapse.js"></script>
    <script src="../assets/js/bootstrap-carousel.js"></script>
    <script src="../assets/js/bootstrap-typeahead.js"></script>
    <script type="text/javascript">
        (function() {
            $("[rel=popover]").popover({
	  placement: 'top' });
        })();
    </script>

  </body>
</html>

END;

?>
