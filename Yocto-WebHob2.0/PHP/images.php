<?php

echo <<< END

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- 
    <meta charset="utf-8">
    <title>Yocto Web Hob</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    -->

    <!-- Le styles -->
    <!-- 
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link href="css/yocto.css" rel="stylesheet">
    <link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">
    -->

    <!-- Le fav and touch icons -->
    <!-- 
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
    -->
     
  
     
  </head>

<body>

END;

	if (isset($_REQUEST['selectClass2'])) {
		$selectClass2 = $_REQUEST['selectClass2'];
	} else {
		$selectClass2 = "";
	}
    
    //FUTURE: add dynamic info from wikipedia
	
	//For the moment a static earch is done:	
	switch ($selectClass2) 
	{
		case "rpi-basic-image":
			echo '<p>Good starting point for your build project with Raspberry Pi. Based on <i>"rpi-hwup-image"</i> image.<br />';
			echo '<a href="#">Read more about this rpi-basic-image &#187;</a></p></div><!--right-->';
			break;
		case "rpi-hwup-image":
			echo '<p>Good starting point for your build project with Raspberry Pi. Based on <i>"core-image-minimal"</i> image.<br />';
			echo '<a href="#">Read more about this rpi-hwup-image &#187;</a></p></div><!--right-->';
			break;
		case "qt4e-demo-image":
			echo '<p>An image that will launch into the demo application for the embedded (not based on X11) version of Qt.<br />';
			echo '<a href="#">Read more about this qt4e-demo-image &#187;</a></p></div><!--right-->';
			break;
		case "core-image-rt":
			echo '<p>A small image just capable of allowing a device to boot plus a real-time test suite and tools appropriate for real-time use.<br />';
			echo '<a href="#">Read more about this core-image-rt &#187;</a></p></div><!--right-->';
			break;
		case "core-image-rt-sdk":
			echo '<p>Small image capable of booting a device with a test suite and tools for real-time use. It includes the full meta-toolchain, development headers and libraries to form a standalone SDK.<br />';
			echo '<a href="#">Read more about this core-image-rt-sdk &#187;</a></p></div><!--right-->';
			break;			
		case "core-image-basic":
			echo '<p>A foundational basic image without support for X that can be reasonably used for customization.<br />';
			echo '<a href="#">Read more about this core-image-basic &#187;</a></p></div><!--right-->';
			break;		
		case "core-image-core":
			echo '<p>An X11 image with simple applications such as terminal, editor, and file manager.<br />';
			echo '<a href="#">Read more about this core-image-core &#187;</a></p></div><!--right-->';
			break;			
		case "core-image-base":		
			echo '<p>A console-only image that fully supports the target device hardware.<br />';
			echo '<a href="#">Read more about this core-image-base &#187;</a></p></div><!--right-->';
			break;		
		case "core-image-minimal":
			echo '<p>A small image just capable of allowing a device to boot.<br />';
			echo '<a href="#">Read more about this core-image-minimal &#187;</a></p></div><!--right-->';
			break;			
		case "core-image-lsb":
			echo '<p>A foundational basic image without support for X that can be reasonably used for customization and is suitable for implementations that conform to Linux Standard Base (LSB).<br />';
			echo '<a href="#">Read more about this core-image-lsb &#187;</a></p></div><!--right-->';
			break;			
		case "core-image-sato":		
			echo '<p>Image with Sato, a mobile environment and visual style for mobile devices. The image supports X11 with a Sato theme, Pimlico applications, and contains terminal, editor, and file manager.<br />';
			echo '<a href="#">Read more about this core-image-sato &#187;</a></p></div><!--right-->';
			break;			
		case "core-image-sato-dev":
			echo '<p>Image with Sato for development work. It includes everything within core-image-sato plus a native toolchain, application development and testing libraries, profiling and debug symbols.<br />';
			echo '<a href="#">Read more about this core-image-sato-dev &#187;</a></p></div><!--right-->';
			break;			
		case "core-image-sato-sdk":
			echo '<p>Image with Sato support that includes everything within core-image-sato plus meta-toolchain, development headers and libraries to form a standalone SDK.<br />';
			echo '<a href="#">Read more about this core-image-sato-sdk &#187;</a></p></div><!--right-->';
			break;			
		case "core-image-lsb-sdk":		
			echo '<p>Basic image without X support suitable for Linux Standard Base (LSB) implementations. It includes the full meta-toolchain, plus development headers and libraries to form a standalone SDK.<br />';
			echo '<a href="#">Read more about this core-image-lsb-sdk &#187;</a></p></div><!--right-->';
			break;			
		case "core-image-lsb-dev":
			echo '<p>Basic image without X support suitable for development work. It can be used for customization and implementations that conform to Linux Standard Base (LSB).<br />';
			echo '<a href="#">Read more about this core-image-lsb-dev &#187;</a></p></div><!--right-->';
			break;			
		case "build-appliance-image":
			echo '<p>An image you can boot and run using either the VMware Player or VMware Workstation.<br />';
			echo '<a href="http://www.yoctoproject.org/documentation/build-appliance">Read more about this build-appliance-image &#187;</a></p></div><!--right-->';
			break;			
		case "core-image-minimal-dev":		
			echo '<p>A small image just capable of allowing a device to boot and is suitable for development work.<br />';
			echo '<a href="#">Read more about this core-image-minimal-dev &#187;</a></p></div><!--right-->';
			break;			
		case "core-image-minimal-initramfs":
			echo '<p>Small image capable of booting a device. The kernel includes the Minimal RAM-based Initial Root Filesystem (initramfs), which finds the first “init” program more efficiently.<br />';
			echo '<a href="#">Read more about this core-image-minimal-initramfs &#187;</a></p></div><!--right-->';
			break;
		case "core-image-minimal-mtdutils":
			echo '<p>Small image capable of booting a device with support for the Minimal MTD Utilities, which let the user interact with the MTD subsystem in the kernel to perform operations on flash devices.<br />';
			echo '<a href="#">Read more about this core-image-minimal-mtdutils &#187;</a></p></div><!--right-->';
			break;			
		case "core-image-clutter":		
			echo '<p>An image with support for the Open GL-based toolkit Clutter, which enables development of rich and animated graphical user interfaces.<br />';
			echo '<a href="#">Read more about this core-image-clutter &#187;</a></p></div><!--right-->';
			break;			
		case "core-image-gtk-directfb":
			echo '<p>An image capable of running gtk over directfb.<br />';
			echo '<a href="#">Read more about this core-image-gtk-directfb &#187;</a></p></div><!--right-->';
			break;			
		case "u-boot-mkimage_2011":
			echo '<p>U-boot bootloader mkimage tool. u-boot-mkimage_2011.03 u-boot-mkimage_2011.06 versions available.<br />';
			echo '<a href="#">Read more about this u-boot-mkimage_2011.06 &#187;</a></p></div><!--right-->';
			break;
			
		case "u-boot-mkimage_2012":		
			echo '<p>U-boot bootloader mkimage tool. u-boot-mkimage_2012.04.01 version available.<br />';
			echo '<a href="#">Read more about this u-boot-mkimage_2012 &#187;</a></p></div><!--right-->';
			break;
		case "xcb-util-image_0":
			echo '<p>An image which provides additional extensions with the XCB library.<br />';
			echo '<a href="#">Read more about this xcb-util-image_0 &#187;</a></p></div><!--right-->';
			break;
		case "mkelfimage_svn":
			echo '<p>An image that provieds a utility to create ELF boot images from Linux kernel images.<br />';
			echo '<a href="http://www.coreboot.org/Mkelfimage">Read more about this mkelfimage_svn &#187;</a></p></div><!--right-->';
			break;
		default:
			echo '<p>Devices not recognized. No information available...<br />';
			break;
	}

echo <<< END

  </body>
</html>

END;

?>
