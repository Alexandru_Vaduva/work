<?php

/*
error_reporting(E_ALL); 
ini_set('display_errors','1'); 

include "test.php";

global $projects;
global $projName;
*/
echo <<< END

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Yocto Web Hob</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
<!--remove and use @import in css in production-->
<link href="../assets/css/bootstrap.css" rel="stylesheet">
<!--remove and use @import in css in production-->
<link href="css/yocto.css" rel="stylesheet">
<link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">
<!--[if !IE 7]>
<style type="text/css">
#wrap {display:table;height:100%}
</style>
<![endif]-->

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="../assets/ico/favicon.ico">

</head>

<body>

<div id="wrap">

	<!--HEADER-->
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">	
 				<a class="brand" href="home.php?page=index_dashboard">&nbsp;</a>
			
				<ul class="nav" style="text-align:center;">
					<!--SET THE ACTIVE SECTION by adding class="active"-->
					<li><a href="home.php?page=builds" class="icon-builds" value = >Builds</a></li>
					<li><a href="home.php?page=projects" class="icon-projects">Projects</a></li>
					<!-- <li><a href="home.php?page=groups" class="icon-groups">Groups</a></li> -->
				</ul>
		
			
             <!--Top Right Tools-->
             <div id="top-right-tools">
           		<ul class="nav">	
					<li style="visibility:hidden;"><a href="#"><img src="images/icon_search.png" alt="Search" title="Search" /></a></li>
					<li><a data-toggle="modal" href="#queueModal">
						<span class="badge badge-success" style="float:right;margin-left:-2px;">2</span>
						<img src="images/icon_runningbuilds.png" alt="Queue &mdash; 2 builds in progress" title="Queue &mdash; 2 builds in progress" />
						</a></li>
					<li class="dropdown"><a href="#" class="dropdown dropdown-toggle" data-toggle="dropdown" style="width:48px; height:48px; margin-top:-8px; background-image:url('images/icon_user.png');">
						<span class="caret"> </span>
						</a>
						<ul class="dropdown-menu pull-right">
                            <li><img src="images/icon_user.png">&nbsp; <strong>John Doe</strong></li>
                            <li class="divider"></li>
                            <li><a href = "#">Settings</a></li>
                            <li><a href = "#">Account details</a></li>
                            <li class="divider"></li>
                            <li><button class="btn btn-primary" href="home.php?page=index">Log Out</button></li>
	                    </ul>
					</li>
				</ul>
			</div>
			<!--Top Right Tools-->
			</div>
		</div>	
			
	</div>
	<!--HEADER-->
        
END;
	
	echo '<div id="main" class="container"><div class="row main"><div class="span9">';
	echo '<h1 class="inline"><img src="images/icon_builds_48.png" alt="" /> Builds</h1>';
	echo '<ul class="nav nav-pills pull-right match-h1"><li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Sort by date <b class="caret"></b></a>';
	echo '<ul class="dropdown-menu"><li><a href="#">Sort by date</a></li>';
	echo '<li><a href="#">Sort by Project</a></li>';
	echo '<li><a href="#">Sort by Group</a></li></ul></li>';
	echo '<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Filter by Project <b class="caret"></b></a>';
	echo '<ul class="dropdown-menu"><li><a href="#">Filter by Project</a></li>';
	echo '<li><a href="#">Filter by complete builds</a></li>';
	echo '<li><a href="#">Filter by builds in progress</a></li></ul></li></ul>';
	
	/*  <!--MY BUILDS--> */
	echo '<div class="row"><div class="span9"><div class="alert alert-success">';
	echo '<strong>All done! </strong>You have successfully made an image with everything you picked.<button type="button" class="close" data-dismiss="alert">&times;</button></div> ';
	
	/* <!--BUILD STATUSES--><!--Build Status1--> */
	echo '<p><i class="icon-book"></i> core-image-minimal-atom-pc=11233768765</p><div class="row">';
	echo '<div class="span4"><div class="progress progress-success">';
	echo '<div class="bar" style="width: 100%;">COMPLETE!</div></div></div>';
	echo '<div class="span2 copy12"><strong>3.2MB</strong></div><div class="span3">';
	echo '<span class="pull-right"><a data-toggle="modal" href="#build_cancelModal"><i class="icon-trash"></i></a></span></div></div>';
	/* <!--Build Status1--><!--Build Status2--> */
	echo '<p><i class="icon-book"></i> core-image-minimal-atom-pc=11233768765</p><div class="row">';
	echo '<div class="span4"><div class="progress"><div class="bar" style="width: 60%;">12 minutes left</div>';
	echo '</div></div><div class="span2 copy12">estimated size <strong>3.19MB</strong></div>';
	echo '<div class="span3 copy12"><i class="icon-time"></i> <strong>12 minutes</strong> left';
	echo '<span class="pull-right"><a data-toggle="modal" href="#build_cancelModal"><i class="icon-trash"></i></a></span></div></div>';
	/* <!--Build Status2--><!--Build Status3--> */
	echo '<p><i class="icon-book"></i> core-image-minimal-atom-pc=11233768765</p><div class="row">';
	echo '<div class="span4"><div class="progress"><div class="bar" style="width: 40%;">45 minutes left</div>';
	echo '</div></div><div class="span2 copy12">estimated size <strong>3.13MB</strong></div>';
	echo '<div class="span3  copy12"><i class="icon-time"></i> <strong>45 minutes</strong> left';
	echo '<span class="pull-right"><a data-toggle="modal" href="#build_cancelModal"><i class="icon-trash"></i></a></span></div></div>';
	
	/* <!--BUILD STATUSES-->
		<!--MY BUILDS--> */
	echo '</div></div><div class="accordion" id="accordion1">';
                    
    /* <!--accordion1--> */
	echo '<table class="build_table">';
	$nr_projects = sizeof($projects->list->item);
	for ($i = 0; $i < $nr_projects; $i++)
	{
		if (strcmp($projects->list->item[$i]->name, "YoctoBlockRollout") == 0)
			break; //TODO remove and test for /tmp/deploy/images existance
		$loc = '../XML/'.$projects->list->item[$i]->name.'.xml';
		$proj = simplexml_load_file($loc);
		$proj = simplexml_load_string($proj->asXML());
		
		//Add the images name information
		$build = '../python/show_images.py';
		$output = shell_exec($build);

		foreach (preg_split('/],\s*\[/', trim($output, '[]')) as $row) 
		{
			$data = preg_split("/',\s*'/", trim($row, "'"));
			//print_r($data);
		}
		
		$size = sizeof($data);
		$images = array();
		for ($i = 0; $i < $size; $i++)
		{
			$data[$i] = str_replace("']", "", $data[$i]);
			array_push($images, $data[$i]);
		}
		
		//Add the images size information
		$build = '../python/show_images_size.py';
		$output = shell_exec($build);

		foreach (preg_split('/],\s*\[/', trim($output, '[]')) as $row) 
		{
			$data = preg_split("/',\s*'/", trim($row, "'"));
			//print_r($data);
		}
		
		$size = sizeof($data);
		$sizes = array();
		for ($i = 0; $i < $size; $i++)
		{
			$data[$i] = str_replace("']", "", $data[$i]);
			array_push($sizes, $data[$i]);
		}	
		
		$images_number = sizeof($images);
		for ($j = 0; $j < $images_number; $j++)
		{        
			$images[$j] = trim($images[$j]);
			$sizes[$j] = trim($sizes[$j]);
			echo '<tr><td class="col_one"><a href="functions/download.php?filename='.$images[$j].'" class="links">'.$images[$j].'</a></td>';
			echo '<td class="col_two"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne">'.$sizes[$j].'</a></td>';
			echo '<td class="col_three">'.$proj->setup->builds->item[$j]->time.'</td>'; 
			echo '<td class="col_four"><span class="label">'.$proj->setup->name.'</span></td>';
			echo '<td class="col_five"><i class="icon-user"></i> <a href="#">'.$proj->setup->builds->item[$j]->person.'</a></td></tr>';
		}
	}
	echo '</table>';	
	
echo <<< END
			
			<!-- Modify this as to dowload the completed build images that will appear in the table section -->
			<!-- Those modifications are taken from the tmp/deploy/images file -->
<!-- Collapse Download images functionality -->
					<div id="collapse" class="accordion-body collapse">
                         <div class="accordion-inner well">
                         
                              <ul class="nav nav-pills">
                              <li class="active">
                              <a href="#">Download</a>
                              </li>
                              <li><a href="#">Deploy</a></li>
                              <li><a href="#">Test</a></li>
                              </ul>
                              
                              <h1>Download</h1>
                              
                              <form>
                              <table class="table table-bordered table-striped">
                              <tr>
                              <td style="text-align:center;">
                            <div class="controls">
                            <input type="checkbox" id="optionsCheckbox" value="option1">
                            </div>
                              </td>
                              <td><i class="icon-download-alt"></i> <a href="#">core-image-minimal-atom-pc-11233768765.rootfs.jffs2</a></td>
                              <td>3.2 MB</td>
                              </tr>
                              <tr>
                              <td style="text-align:center;">
                            <div class="controls">
                            <input type="checkbox" id="optionsCheckbox" value="option1">
                            </div>
                              </td>
                              <td><i class="icon-download-alt"></i> <a href="#">core-image-minimal-atom-pc-11233768765.rootfs.jffs2</a></td>
                              <td>3.2 MB</td>
                              </tr>
                              <tr>
                              <td style="text-align:center;">
                            <div class="controls">
                            <input type="checkbox" id="optionsCheckbox" value="option1">
                            </div>
                              </td>
                              <td><i class="icon-download-alt"></i> <a href="#">core-image-minimal-atom-pc-11233768765.rootfs.jffs2</a></td>
                              <td>3.2 MB</td>
                              </tr>
                              <tr>
                              <td style="text-align:center;">
                            <div class="controls">
                            <input type="checkbox" id="optionsCheckbox" value="option1">
                            </div>
                              </td>
                              <td><i class="icon-download-alt"></i> <a href="#">core-image-minimal-atom-pc-11233768765.rootfs.jffs2</a></td>
                              <td>3.2 MB</td>
                              </tr>
                              <tr>
                              <td style="text-align:center;">
                            <div class="controls">
                            <input type="checkbox" id="optionsCheckbox" value="option1">
                            </div>
                              </td>
                              <td><i class="icon-download-alt"></i> <a href="#">core-image-minimal-atom-pc-11233768765.rootfs.jffs2</a></td>
                              <td>3.2 MB</td>
                              </tr>
                              <tr>
                              <td style="text-align:center;">
                            <div class="controls">
                            <input type="checkbox" id="optionsCheckbox" value="option1">
                            </div>
                              </td>
                              <td><i class="icon-download-alt"></i> <a href="#">core-image-minimal-atom-pc-11233768765.rootfs.jffs2</a></td>
                              <td>3.2 MB</td>
                              </tr>
                              </table>
                              </form>
                              
                              <div><button class="btn btn-primary btn-large"><i class="icon-download icon-white"></i>Download</button> <a href="#"><i class=" icon-remove icon-blue"></i> Cancel</a> &nbsp;<span class="pull-right match-h3"><input type="checkbox" id="optionsCheckbox" value="option1"> All available files as .zip
                              
                              </div>
                         
                         </div>   
                    </div>
                    <!--accordion2-->
                    
                    
                    
               </div>
               <!--accordion all-->    
             
          </div>
          
          <div class="span3">
               <div class="well">
               <h3>Your Builds</h3>
               <p>The user is presented here with the progress of his latest builds and the possibility to dowload the information available from the finished ones.</p>
               <p>The builds presented have the possibility to be stopped by pressing the <i>recycle bin</i> from the right side.<br>For more information check: <a href="http://www.yoctoproject.org/docs/1.1/yocto-project-qs/yocto-project-qs.html">Building an Image</a> link.</p>
               </div>
               
          </div> 
          
     </div>
<!-- Collapse Download images functionality -->

<!--Queue Modal Content-->
<div id="queueModal" class="modal hide">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h3>Running Builds</h3>
            </div>
            <div class="modal-body">
              <h4>Build 1</h4>
              <p>Some information about the build.</p>
              
              <div class="progress progress-success">
                    <div class="bar" style="width: 60%;"></div>
              </div>
              
              <div class="alert alert-success">Done! <a href="#">Download Build 1 here.</a></div>

              <h4>Build 2</h4>
              <p>Some information about the build.</p>
              
              <div class="progress progress-striped
     active">
                    <div class="bar " style="width: 60%;"></div>
              </div>
              
              <p><i class="icon-time"></i> 01:23:02 remaining</p>

              <h4>Build 3</h4>
              <p>Some information about the build.</p>

              <div class="progress progress-striped
     active">
                    <div class="bar " style="width: 60%;"></div>
              </div>
              
              <p><i class="icon-time"></i> 01:23:02 remaining</p>

              
            </div>
            <div class="modal-footer">
              <a href="#" class="btn" data-dismiss="modal" >Close</a>
            </div>
          </div>
<!--Queue Modal Content-->

<!--Build Report Modal Content-->
<div id="build_reportModal" class="modal hide">

     <div class="modal-header">
     <button type="button" class="close" data-dismiss="modal">&times;</button>
     <h3>Report a build configuration</h3>
     </div>
     
     <div class="modal-body">
          <div class="alert alert-error">
          <strong>Hey!</strong> Reporting false bad configurations can screw other people up.  Be careful.<button type="button" class="close" data-dismiss="alert">&times;</button>
          </div>
              
          <form>
          <label class="control-label" for="textarea01">Additional info about what went wrong</label>
          <textarea class="input-textarea_modal" id="textarea01" rows="5"></textarea>
          
          <h3 style="margin-top:18px;">Severity of the problem</h3>
          
          <div class="control-group">
          
               <div class="controls">
               <label class="radio">
               <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
               Critical
               </label>
               <label class="radio">
               <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
               Blocker
               </label>
                <label class="radio">
               <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
               Medium
               </label>
                 <label class="radio">
               <input type="radio" name="optionsRadios" id="optionsRadios4" value="option4">
               Low
               </label>
               </div>
               
          </div>
          
          </form>
	</div>
    
	<div class="modal-footer">
		<button  href="#" class="btn" data-dismiss="modal">Cancel</button>
    	<button type="submit" class="btn btn-primary">Report</button>
	</div>

</div>
<!--Build Report Modal Content-->

<!--Share Build Modal Content-->
<div id="build_shareModal" class="modal hide">

     <div class="modal-header">
     <button type="button" class="close" data-dismiss="modal">&times;</button>
     <h3>Share your build</h3>
     </div>
     
     <div class="modal-body">
          <div class="alert alert-info">
          <strong>Share what you made!</strong> Just put in someone's email and they'll get a lnk.<button type="button" class="close" data-dismiss="alert">&times;</button>
          </div>
              
          <form>
          <input type="text" class="span3" placeholder="Email address of recipient">
          <label class="control-label" for="textarea01">Message</label>
          <textarea class="input-textarea_modal" id="textarea01" rows="3"></textarea>
          </form>
     </div>

	<div class="modal-footer">
		<button  href="#" class="btn" data-dismiss="modal">Cancel</button>
	    <button type="submit" class="btn btn-primary">Send</button>
	</div>
</div>
<!--Share Build  Modal Content-->


<!--Cancel Build Modal Content-->
<div id="build_cancelModal" class="modal hide">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h3>Cancel your build</h3>
	</div>     
	<div class="modal-body">
		<div class="alert alert-info">
		<strong>Are you sure?</strong> You can't undo deleting a build.<button type="button" class="close" data-dismiss="alert">&times;</button>
	</div>
	</div>
	<div class="modal-footer">
		<button  href="#" class="btn" data-dismiss="modal">No</button>
    	<button type="submit" class="btn btn-primary">Yes</button>
	</div>
</div>
<!--Cancel Build Modal Content-->





</div> <!-- /container -->
</div>

<footer>
	<div class="container" >
		<div class="row">
			<div class="span3" style="opacity:.65;">
				<p>&copy; 2012 The Yocto Project</p>
			</div>
			<div class="span3">
					<a href="#">About</a>
					<a href="#">Blogs</a>
					<a href="#">Documentation</a>
			</div>
			<div class="span3">
				<a href="#">Privacy Policy</a>
				<a href="#">Terms of Service</a>
				<a href="#">Trademarks</a>
			</div>
			<div class="span3">
				<a href="http://www.linuxfoundation.org" style="height:66px"><img src="images/linux_foundation.png" alt="Linux Foundation"/></a>
			</div>
		</div>
	</div>
</footer>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap-transition.js"></script>
    <script src="../assets/js/bootstrap-alert.js"></script>
    <script src="../assets/js/bootstrap-modal.js"></script>
    <script src="../assets/js/bootstrap-dropdown.js"></script>
    <script src="../assets/js/bootstrap-scrollspy.js"></script>
    <script src="../assets/js/bootstrap-tab.js"></script>
    <script src="../assets/js/bootstrap-tooltip.js"></script>
    <script src="../assets/js/bootstrap-popover.js"></script>
    <script src="../assets/js/bootstrap-button.js"></script>
    <script src="../assets/js/bootstrap-collapse.js"></script>
    <script src="../assets/js/bootstrap-carousel.js"></script>
    <script src="../assets/js/bootstrap-typeahead.js"></script>

  </body>
</html>

END;

?>
