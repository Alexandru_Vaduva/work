<?php
/*
error_reporting(E_ALL); 
ini_set('display_errors','1'); 

include "test.php";

global $projects;
global $projName;

//echo "<br>".$nr_builds."<br>";
//echo "<br>".getProjectName(0)."<br>";
*/

echo <<< END

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Yocto Web Hob</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
<!--remove and use @import in css in production-->
<link href="../assets/css/bootstrap.css" rel="stylesheet">
<!--remove and use @import in css in production-->
<link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">
<link href="css/yocto.css" rel="stylesheet">

<!--[if !IE 7]>
<style type="text/css">
#wrap {display:table;height:100%}
</style>
<![endif]-->

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="../assets/ico/favicon.ico">

<!-- Script for selecting machines -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.js"></script>
<script type="text/javascript">
	$(function() 
	{
		$("#selectClass").change(function()
		{
			var selectedValue = $(this).find(":selected").text();
			
			$("#machines").html("Please wait ...");
			
			$.ajax ({
				url: "machines.php",
				type: "GET",
				data: {selectClass: selectedValue},
				async: true,
				success: function (data)
				{
					$("#machines").html(data);
				},
			});			
			
			console.log("the value you selected: " + selectedValue);
		});
	});
</script>

<!-- Script for selecting base images -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.js"></script>
<script type="text/javascript">
	$(function() 
	{
		$("#selectClass2").change(function()
		{
			var selectedValue = $(this).find(":selected").text();
			
			$("#images").html("Please wait ...");
			
			$.ajax ({
				url: "images.php",
				type: "GET",
				data: {selectClass2: selectedValue},
				async: true,
				success: function (data)
				{
					$("#images").html(data);
				},
			});
			
			console.log("the value you selected: " + selectedValue);
		});
	});
</script>

</head>

<body>

<div id="wrap">

	<!--HEADER-->
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">	
 				<a class="brand" href="home.php?page=index_dashboard">&nbsp;</a>
			
				<ul class="nav" style="text-align:center;">
					<!--SET THE ACTIVE SECTION by adding class="active"-->
					<li><a href="home.php?page=builds" class="icon-builds">Builds</a></li>
					<li class="active"><a href="home.php?page=projects" class="icon-projects">Projects</a></li>
					<!-- <li><a href="home.php?page=groups" class="icon-groups">Groups</a></li> -->
				</ul>
		
			
             <!--Top Right Tools-->
             <div id="top-right-tools">
           		<ul class="nav">	
					<li style="visibility:hidden;"><a href="#"><img src="images/icon_search.png" alt="Search" title="Search" /></a></li>
					<li><a data-toggle="modal" href="#queueModal">
						<span class="badge badge-success" style="float:right;margin-left:-2px;">2</span>
						<img src="images/icon_runningbuilds.png" alt="Queue &mdash; 2 builds in progress" title="Queue &mdash; 2 builds in progress" />
						</a></li>
					<li class="dropdown"><a href="#" class="dropdown dropdown-toggle" data-toggle="dropdown" style="width:48px; height:48px; margin-top:-8px; background-image:url('images/icon_user.png');">
						<span class="caret"> </span>
						</a>
						<ul class="dropdown-menu pull-right">
                            <li><img src="images/icon_user.png">&nbsp; <strong>John Doe</strong></li>
                            <li class="divider"></li>
                            <li><a href = "#">Settings</a></li>
                            <li><a href = "#">Account details</a></li>
                            <li class="divider"></li>
                            <li><button class="btn btn-primary" href="home.php?page=index">Log Out</button></li>
	                    </ul>
					</li>
				</ul>
			</div>
			<!--Top Right Tools-->
			</div>
		</div>	
			
	</div>
	<!--HEADER-->

     <div id="main" class="container" >

          <!-- Main Content -->
          <!--Top Bar-->
         <h1><span class="glyph enclosed" style="vertical-align:top;">c</span> Projects</h1>        
           
END;
	echo ' <div class="row"><div class="span12" >';
	echo '<h2 class="pull-left">'.$projName->setup->name.'</h2>';
	echo '<ul class="nav nav-pills topbarnav pull-left" style="margin-left:0;">';
	echo '<li class="active"><a href="home.php?page=projects_myproject">Project</a>';
	echo '</li><li><a href="home.php?page=projects_history"><i class="icon-time icon-blue"></i> History</a></li>';
	echo '<li><a href="home.php?page=projects_settings"><i class="icon-cog icon-blue"></i> Settings</a></li></ul>';
	//echo '<li><a href="home.php?page=projects_settings"><i class="icon-cog icon-blue"></i> Settings</a></li>';
	//echo '<li><a href="home.php?page=projects_permissions"><i class="icon-lock icon-blue"></i> Permissions</a></li></ul>';
	//<!--Top Bar--><!--MAIN WORK AREA-->
	echo '</div></div><div class="row"><div class="span9">';

	echo '<!--CREATE A BUILD-->';
	echo '<!--WELL--><div class="">';
	echo '<!--CAB Header--><div class="row"><div class="span12">';
	echo '<h2 style="position:relative;top: 50%; left: 20%;">Create a build</h2></div> </div><!--CAB Header-->';
	echo '<!--CAB MAIN--><div class="row">';
	echo '<!--LeftSide--><div class="span7">';
	
	echo '<!--SELECT MACHINE--><!--header-->';
	echo '<div class="row"><div class="span7">';
	echo '<h3>Select a machine</h3></div></div>';
	echo '<!--header--><!--content--><div class="row">';
	
	echo '<!--left--><div class="span3">';
	echo '<form class="form"><fieldset>';
	echo '<div class="controls"><select id="selectClass">';
		
	$build = '../python/show_layers_machines.py';
	$output = shell_exec($build);

	foreach (preg_split('/],\s*\[/', trim($output, '[]')) as $row) 
	{
        $data = preg_split("/',\s*'/", trim($row, "'"));
        //print_r($data);
	}
	
	$size = sizeof($data);
	$machines = array();
	for ($i = 0; $i < $size; $i++)
	{
		$data[$i] = str_replace("']", "", $data[$i]);
		array_push($machines, $data[$i]);
	}
	$machines_number = sizeof($machines);
	
	$nr_boards = sizeof($projName->setup->machines->item);
	for ($j = 0; $j < $machines_number; $j++)
	{
		$checked = false;
		for ($i = 0; $i < $nr_boards; $i++)
		{
			if (strncmp($machines[$j], $projName->setup->machines->item[$i]->name, strlen($projName->setup->machines->item[$i]->name)) == 0)
			{
				$checked = true;
				break;
			}
		}
		if ($checked == true)
		{
			echo '<option value="'.$projName->setup->machines->item[$i]->name.'" selected="selected">'.$projName->setup->machines->item[$i]->name.'</option>';
			$checked = false;
		}
		else
		{
			$machines[$j] = trim($machines[$j]);
			echo '<option value="'.$machines[$j].'">'.$machines[$j].'</option>';
		}
	}
	echo '</select></div></fieldset></form></div><!--left-->';
			
	echo '<!--right--><div class="span4" id="machines">';
    echo '<img src="'.$projName->setup->machines->item[0]->image.'" width="170" height="30" alt="RaspberryPi Logo" />';
    echo '<p>'.$projName->setup->machines->item[0]->details.'<br />';
    echo '<a href="'.$projName->setup->machines->item[0]->website.'">'.$projName->setup->machines->item[0]->name.' website &#187;</a></p></div><!--right-->';   
                
    echo '</div><!--content--><!--SELECT MACHINE-->';
    echo '<!--SELECT BASE IMG--><!--header--><div class="row"><div class="span7">';
    echo '<h3>Select a base image</h3></div></div><!--header--><!--content--><div class="row">';
                
    echo '<!--left--><div class="span3"><form class="form"><fieldset>';
    echo '<fieldset><div class="controls"><select id="selectClass2">';
    
    $build = '../python/show_layers_images.py';
	$output = shell_exec($build);

	foreach (preg_split('/],\s*\[/', trim($output, '[]')) as $row) 
	{
        $data = preg_split("/',\s*'/", trim($row, "'"));
        //print_r($data);
	}
	
	$size = sizeof($data);
	$images = array();
	for ($i = 0; $i < $size; $i++)
	{
		$data[$i] = str_replace("']", "", $data[$i]);
		array_push($images, $data[$i]);
	}
	
	$images_number = sizeof($images);                
    $nr_images = sizeof($projName->setup->images->base_image);
	for ($j = 0; $j < $images_number; $j++)
	{
		$checked = false;
		for ($i = 0; $i < $nr_images; $i++)
		{
			if (strncmp($images[$j], $projName->setup->images->base_image[$i], strlen($projName->setup->images->base_image[$i])) == 0)
			{
				$checked = true;
				break;
			}
		}
		if ($checked == true)
		{
			echo '<option value="'.$projName->setup->images->base_image[$i].'" selected="selected">'.$projName->setup->images->base_image[$i].'</option>';
			$checked = false;
		}
		else
		{
			$images[$j] = trim($images[$j]);
			echo '<option value="'.$images[$j].'">'.$images[$j].'</option>';
		}
	}
	echo '</select></div></fieldset></form></div><!--left-->';
	echo '<!--right--><div class="span4" id="images">';
	echo '<p>Good starting point for your build project with Raspberry Pi. Based on <i>"rpi-hwup-image"</i> image.<br />';
	echo '<a href="#">Read more about this '.$projName->setup->images->base_image[0].' &#187;</a></p></div><!--right-->';
				
	echo '</div><!--content--><!--SELECT BASE IMG-->';
	echo '<div class="alert alert-info"><strong>Heads up!</strong> If you are all ready, you can go straight to building your image. If you want a little more customisation, then you should build your packages first.<a class="close" data-dismiss="alert" href="#">&times;</a></div><hr />';
			
	echo '<div class="row">';
	echo '<!--CAB Sidebar--><div class="span4 well2">';
	echo '<ul class="nav nav-pills pull-right">';
	echo '<li class="dropdown" style="margin-top: -16px;">';
	echo '<a class="dropdown-toggle" data-toggle="dropdown" href="#">Most recent <b class="caret"></b></a>';
	echo '<ul class="dropdown-menu" style="margin-top: 5px;">';
	echo '<li><a href="#">Option 1</a></li>';
	echo '<li><a href="#">Option 2</a></li>';
	echo '</ul></li></ul>';
						
	echo '<h2 style="width:150px;">Add Layers</h2>';
	echo '<div class="row"><div class="span4"><form class="form-inline">';
	echo '<input type="text" class="input-small" placeholder="Search..." style="width:95%;" />';
	echo '</form></div></div><div class="row">';
	echo '<div class="span4 pull-left"><form class="form"><table class="table">';
	
	$nr_layers = sizeof($projName->setup->layers->item);
	//echo "<br>".$nr_layers."<br>";
	for ($i = 0; $i < $nr_layers; $i++)
	{
		echo '<tr class="disable"><td><label class="checkbox">';
		echo '<input type="checkbox" id="a_optionsCheckbox1" value="a_option1" disabled checked>'.$projName->setup->layers->item[$i]->name.'</label></td>';
		echo '<td><label class="control-label" for="a_optionsCheckbox1">'.$projName->setup->layers->item[$i]->utility.'</label></td><td></td>';
		if (strcmp($projName->setup->layers->item[$i]->utility, "Required") != 0)
		{
			echo '<td><i class="icon-trash"></i></td>';
		}
			echo '</tr>';
	}
	echo '</table></form></div></div>';
	echo '<div class="row"><div class="span4"><div class="pagination" style="margin:-20px 0 50px 0; text-align:center;">';
	if ($nr_layers > 10)
	{
		echo '<ul><li class="disabled"><a href="#">«</a></li>';
		for ($i = 1; $i < ($nr_layers+10)/10; $i++)
		{
			if ($i == 1)
				echo '<li class="active"><a href="#">'.$i.'</a></li>';
			else
				echo '<li><a href="#">'.$i.'</a></li>';
		}
		echo '<li><a href="#">»</a></li></ul>';
	}
	echo '</div><div class="btn-toolbar">';
	echo '<form class="form"><label>Add Layer by URL</label>';
	echo '<input type="text" class="span3" placeholder="URL">';
	echo '<button type="submit" class="btn">Add</button>';
	echo '<label>Add Layer from external file</label>';
	echo '<input type="text" class="span3" placeholder="Browse for file on your computer">';
	echo '<button type="submit" class="btn">Go</button></form></div>';
	echo '</div></div> </div></div>'; 
	
	echo '<div class="row pull-right">';
	echo '<a data-toggle="modal" href="#buildscheduleModal" class="btn btn-primary btn-large">Build Image</a> <a class="btn btn-large" href="home.php?page=createbuild_packagegroups">Build Packages</a>';
	echo '<input type="submit" name="submit" class="btn-primary btn" id="index_dashboard" value="Go">';
	
	echo '</div></div><!--LeftSide-->';
		     
	echo '</div></div>';
                   
   
    echo '</div> <!--MAIN WORK AREA-->';
    echo '<!--SIDEBAR--> <div class="span3">';                    	 
    echo '<div class="span3"><div class="well">';
    echo '<h4><img src="images/icon_faq_36.png" width="24" height="24" alt="" class="glyph" /> How to manage builds?</h4>';
    echo '<p>For building a project a user needs to select the machine, the resulting image and the layers used to obtain functionality for the project.</p>';
    echo '<p>After the selection is done there is also the possibility to build the entire image or to build a specific package for that image.</p>';
    echo '<p><a href="http://www.yoctoproject.org/docs/current/yocto-project-qs/yocto-project-qs.html"><strong>More about build properties &raquo;</strong></a></p></div></div> </div></div>';      
    echo ' <!--SIDEBAR-->  </div>';			
    
	//<!--Main Content-->
	
echo <<< END
            
<!--Queue Modal Content-->
<div id="queueModal" class="modal hide">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h3>Running Builds</h3>
            </div>
            <div class="modal-body">
              <h4>Build 1</h4>
              <p>Some information about the build.</p>
              
              <div class="progress progress-success">
                    <div class="bar" style="width: 60%;"></div>
              </div>
              
              <div class="alert alert-success">Done! <a href="#">Download Build 1 here.</a></div>

              <h4>Build 2</h4>
              <p>Some information about the build.</p>
              
              <div class="progress progress-striped
     active">
                    <div class="bar " style="width: 60%;"></div>
              </div>
              
              <p><i class="icon-time"></i> 01:23:02 remaining</p>

              <h4>Build 3</h4>
              <p>Some information about the build.</p>

              <div class="progress progress-striped
     active">
                    <div class="bar " style="width: 60%;"></div>
              </div>
              
              <p><i class="icon-time"></i> 01:23:02 remaining</p>

              
            </div>
            <div class="modal-footer">
              <a href="#" class="btn" data-dismiss="modal" >Close</a>
            </div>
          </div>
<!--Queue Modal Content-->

<!--Add Person Modal Content-->
<div id="addpersonModal" class="modal hide">
     
     <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
       <h3>Add someone to your project</h3>
     </div>
     
     <div class="modal-body">
	
     <p>Adding someone to your project is easy. They don't have to be a member of your group. Just put in their email, type a message if you like and they'll be sent a message and have an ID automatically created for them.</p>
	
     <form class="form">
     <input type="text" placeholder="Email Address" style="margin:12px 0 12px 0; width:400px;">

	<textarea class="input-textarea_modal" id="textarea02" rows="3" placeholder="Your message" style="height:60px;"></textarea>

     <button type="submit" class="btn" style="margin-top:24px;">Clear</button><br />
     
     </div>
     
     <div class="modal-footer">
	
     <a href="#" class="btn" data-dismiss="modal" >Cancel</a>
     <button class="btn btn-primary"><i class="icon-plus-sign icon-white"></i> Add</button>
     </form>
     
     </div>
     
</div>
<!--Add Person Modal Content-->

END;
	//<!--Build Schedule Modal Content-->
	echo '<div id="buildscheduleModal" class="modal hide">';
	echo '<div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button>';
	echo '<h3>Schedule your build</h3></div>';
	
	echo '<div class="modal-body"><div class="alert alert-info">';
	echo '<strong>Hey!</strong> Just so you know! Your build will run when resources allow so times aren’t exact.<button type="button" class="close" data-dismiss="alert">&times;</button>';
	echo '</div><h4>Image to build</h4>';
	echo '<div class="well2"><p><strong>Machine:</strong> '.$projName->setup->machines->item[0]->name.'</p>';
	echo '<p><strong>Base image:</strong> '.$projName->setup->images->base_image[0].'</p></div>';
	
	//echo '<form action="home.php" method="post" style="display: inline;">';
	//echo '<input type="hidden" name="build" value='.$projName->setup->images->base_image[$bi_idx].'>';
	echo '<a class="brand" href="home.php?page=createbuild_buildimage&build='.$projName->setup->images->base_image[0].'">';
	echo '<button type="submit" class="btn btn-primary" style="margin-top:18px">Build now</button></a>';
	//echo '<button type="submit" class="btn btn-primary" style="margin-top:18px" onclick="startBuild()">Build now</button></a>';
	//echo '</form></div>';	
	
	echo '<hr /><h4>Schedule it for later</h4><form class="form-inline">';
	$today_date = date("d-m-Y");
	$today_time = date("H:i");
	echo '<label>Date </label><input type="text" class="input-small" placeholder='.$today_date.'>';
	echo '<label>Time </label><input type="text" class="input-small" placeholder='.$today_time.'>';
	//echo '</form><img src="images/calendar.png"></div>';
	echo '<div class="modal-footer"><button class="btn btn-primary">Add to Queue now</button>';
	echo '<a href="#" class="btn" data-dismiss="modal" >Cancel</a></div></div>';
	//<!--Build Schedule Modal Content-->
		
echo <<< END

			


    </div></div><!-- /container -->


	<footer>
		<div class="container" >
			<div class="row">
				<div class="span3" style="opacity:.65;">
					<p>&copy; 2012 The Yocto Project</p>
				</div>
				<div class="span3">
						<a href="#">About</a>
						<a href="#">Blogs</a>
						<a href="#">Documentation</a>
				</div>
				<div class="span3">
					<a href="#">Privacy Policy</a>
					<a href="#">Terms of Service</a>
					<a href="#">Trademarks</a>
				</div>
				<div class="span3">
					<a href="http://www.linuxfoundation.org" style="height:66px"><img src="images/linux_foundation.png" alt="Linux Foundation"/></a>
				</div>
			</div>
		</div>
	</footer>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap-transition.js"></script>
    <script src="../assets/js/bootstrap-alert.js"></script>
    <script src="../assets/js/bootstrap-modal.js"></script>
    <script src="../assets/js/bootstrap-dropdown.js"></script>
    <script src="../assets/js/bootstrap-scrollspy.js"></script>
    <script src="../assets/js/bootstrap-tab.js"></script>
    <script src="../assets/js/bootstrap-tooltip.js"></script>
    <script src="../assets/js/bootstrap-popover.js"></script>
    <script src="../assets/js/bootstrap-button.js"></script>
    <script src="../assets/js/bootstrap-collapse.js"></script>
    <script src="../assets/js/bootstrap-carousel.js"></script>
    <script src="../assets/js/bootstrap-typeahead.js"></script>
<script type="text/javascript">
    $("[rel=tooltip]").tooltip();
</script>

  </body>
</html>

END;

?>
