<?php
/*
error_reporting(E_ALL); 
ini_set('display_errors','1'); 

include "test.php";

global $projects;
global $templates;
//echo "<br>".$nr_projects."<br>";
//echo "<br>".getProjectName(0)."<br>";
*/
echo <<< END

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Yocto Web Hob</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
<!--remove and use @import in css in production-->
<link href="../assets/css/bootstrap.css" rel="stylesheet">
<!--remove and use @import in css in production-->
<link href="css/yocto.css" rel="stylesheet">
<link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">
<!--[if !IE 7]>
<style type="text/css">
#wrap {display:table;height:100%}
</style>
<![endif]-->

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="../assets/ico/favicon.ico">

</head>

<body>

<div id="wrap">

	<!--HEADER-->
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">	
 				<a class="brand" href="home.php?page=index_dashboard">&nbsp;</a>
			
				<ul class="nav" style="text-align:center;">
					<!--SET THE ACTIVE SECTION by adding class="active"-->
					<li><a href="home.php?page=builds" class="icon-builds" value = >Builds</a></li>
					<li><a href="home.php?page=projects" class="icon-projects">Projects</a></li>
					<!-- <li><a href="home.php?page=groups" class="icon-groups">Groups</a></li> -->
				</ul>
		
			
             <!--Top Right Tools-->
             <div id="top-right-tools">
           		<ul class="nav">	
					<li style="visibility:hidden;"><a href="#"><img src="images/icon_search.png" alt="Search" title="Search" /></a></li>
					<li><a data-toggle="modal" href="#queueModal">
						<span class="badge badge-success" style="float:right;margin-left:-2px;">2</span>
						<img src="images/icon_runningbuilds.png" alt="Queue &mdash; 2 builds in progress" title="Queue &mdash; 2 builds in progress" />
						</a></li>
					<li class="dropdown"><a href="#" class="dropdown dropdown-toggle" data-toggle="dropdown" style="width:48px; height:48px; margin-top:-8px; background-image:url('images/icon_user.png');">
						<span class="caret"> </span>
						</a>
						<ul class="dropdown-menu pull-right">
                            <li><img src="images/icon_user.png">&nbsp; <strong>John Doe</strong></li>
                            <li class="divider"></li>
                            <li><a href = "#">Settings</a></li>
                            <li><a href = "#">Account details</a></li>
                            <li class="divider"></li>
                            <li><button class="btn btn-primary" href="home.php?page=index">Log Out</button></li>
	                    </ul>
					</li>
				</ul>
			</div>
			<!--Top Right Tools-->
			</div>
		</div>	
			
	</div>
	<!--HEADER-->

     <div id="main" class="container" >

          <!-- Main Content -->
          <!--Top Bar-->
           <div class="row">
               <div class="span12">
                    <h1><span class="glyph enclosed" style="vertical-align:top;">c</span> Projects</h1>
               </div>
          </div>
          <!--Top Bar-->
      
          <div class="row">
          
               <!--MAIN WORK AREA-->
               <div class="span9">
                         
                    <!--Project List-->
              
END;
	
	//Projects enumeration	
	$nr_projects = sizeof($projects->list->item);
	for ($i = 0; $i < $nr_projects; $i++)
	{
		echo '<div class="row">'; 
		echo '<div class="span2">';
		echo '<h4><a href="home.php?page=projects_myproject&project='.$projects->list->item[$i]->name.'">'.$projects->list->item[$i]->name.'</a></h4>';
		echo "</div>";
      
		echo '<div class="span7">';
		echo '<div class="row">';
		echo '<div class="span4">';
		echo '<p>'.$projects->list->item[$i]->info.'</p></div>';
		
		//echo '<div class="span3">';
		
		//echo '</div></div></div>';
		//TODO: Add users functionality and info xml`s.
		//echo sizeof($projects->list->item[$i]->groups->name). " - ";
		/*for ($j = 0; $j < sizeof($projects->list->item[$i]->groups->name); $j++)
		{
			for ($k = 0; $k < getProjectMemberNum($i, $j); $k++)
			{
				echo '<p><a href="#" rel="tooltip" title="';
				echo getProjectMemberName($i, $j, $k);
				echo '" class="tooltip-test"><img src="';
				echo getProjectMemberPicture($i, $j, $k);
				echo '" alt="user name" width="36" height="36" /></a>';	
			}	
		}*/	
		//echo '</div></div></div></div>';
		echo '</div></div></div>';
		echo '<p>&nbsp;</p>';
	} 
	//<!--Create Project-->
	echo '<div class="row">
			<div class="span8 well2">';
	echo '<h2><span class="glyph enclosed">d</span> Create a new project</h2>';
	echo '<form class="form createnewproject" style="padding-top: 24px;">';
	echo '<div><input type="text" class="span3" placeholder="Name your project"></div>';
	echo '<p>&nbsp;</p><div class="control-group"><div class="controls">';
	echo '<select id="select01">';	
	echo '<option>Select a template</option>';
	$nr_templates  = sizeof($templates->list->item);
	for ($i = 0; $i < $nr_templates; $i++)
	{
		echo '<option>'.$templates->list->item[$i]->name.'</option>';
	}
	echo '</select></div></div>';
	
	echo '<span class="pull-left" style="margin-top:4px; margin-right:7px;">or &nbsp; &nbsp; </span>';
	echo '<div class="control-group"><div class="controls"><select id="select01">';
	echo '<option>Duplicate a project</option>';
	$nr_projects = sizeof($projects->list->item);
	for ($i = 0; $i < $nr_projects; $i++)
	{		
		echo '<option>'.$projects->list->item[$i]->name.'</option>';
	}
	echo '</select></div></div>';
	echo '<div style="width:100%;display:inline;float:left; margin-top:18px;"><button type="submit" class="btn btn-primary"><i class="icon-plus-sign icon-white"></i> Create Project</button></div>';
    echo '</form></div></div></div>';
    //<!--Create Project--><!--MAIN WORK AREA-->
     
    //<!--SIDEBAR-->
    echo '<div class="span3"><div class="well">';
    echo '<h4><img src="images/icon_faq_36.png" width="24" height="24" alt="" class="glyph" /> What are projects?</h4>';
    echo '<p>The projects are represented by build directory that is set after the setup environment script is executed by the user.</p>';
    echo '<p><a href="http://www.yoctoproject.org/docs/1.4/dev-manual/dev-manual.html"><strong>More about projects &raquo;</strong></a></p></div></div> </div></div>';
    //<!--SIDEBAR--> <!--Main Content-->
                   
echo <<< END

<!--Queue Modal Content-->
<div id="queueModal" class="modal hide">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h3>Running Builds</h3>
            </div>
            <div class="modal-body">
              <h4>Build 1</h4>
              <p>Some information about the build.</p>
              
              <div class="progress progress-success">
                    <div class="bar" style="width: 60%;"></div>
              </div>
              
              <div class="alert alert-success">Done! <a href="#">Download Build 1 here.</a></div>

              <h4>Build 2</h4>
              <p>Some information about the build.</p>
              
              <div class="progress progress-striped
     active">
                    <div class="bar " style="width: 60%;"></div>
              </div>
              
              <p><i class="icon-time"></i> 01:23:02 remaining</p>

              <h4>Build 3</h4>
              <p>Some information about the build.</p>

              <div class="progress progress-striped
     active">
                    <div class="bar " style="width: 60%;"></div>
              </div>
              
              <p><i class="icon-time"></i> 01:23:02 remaining</p>

              
            </div>
            <div class="modal-footer">
              <a href="#" class="btn" data-dismiss="modal" >Close</a>
            </div>
          </div>
<!--Queue Modal Content-->
    </div><!-- /container -->
</div>

<footer>
	<div class="container" >
		<div class="row">
			<div class="span3" style="opacity:.65;">
				<p>&copy; 2012 The Yocto Project</p>
			</div>
			<div class="span3">
					<a href="#">About</a>
					<a href="#">Blogs</a>
					<a href="#">Documentation</a>
			</div>
			<div class="span3">
				<a href="#">Privacy Policy</a>
				<a href="#">Terms of Service</a>
				<a href="#">Trademarks</a>
			</div>
			<div class="span3">
				<a href="http://www.linuxfoundation.org" style="height:66px"><img src="images/linux_foundation.png" alt="Linux Foundation"/></a>
			</div>
		</div>
	</div>
</footer>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap-transition.js"></script>
    <script src="../assets/js/bootstrap-alert.js"></script>
    <script src="../assets/js/bootstrap-modal.js"></script>
    <script src="../assets/js/bootstrap-dropdown.js"></script>
    <script src="../assets/js/bootstrap-scrollspy.js"></script>
    <script src="../assets/js/bootstrap-tab.js"></script>
    <script src="../assets/js/bootstrap-tooltip.js"></script>
    <script src="../assets/js/bootstrap-popover.js"></script>
    <script src="../assets/js/bootstrap-button.js"></script>
    <script src="../assets/js/bootstrap-collapse.js"></script>
    <script src="../assets/js/bootstrap-carousel.js"></script>
    <script src="../assets/js/bootstrap-typeahead.js"></script>


<script type="text/javascript">
    $("[rel=tooltip]").tooltip();
</script>


  </body>
</html>

END;

?>
