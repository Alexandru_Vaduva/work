<?php

$styeVar = 'style="text-align:center;"';
/*
//echo "Index test 0 ";
if (!$_SESSION['loggedin'])
{
// echo "Index test 1";
   $styeVar = 'style="visibility:hidden;"';
}
*/
echo <<< END

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Yocto Web Hob</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
     <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link href="css/yocto.css" rel="stylesheet">
    <link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
     
  
     
  </head>

<body>

<div id="wrap">

	<!--HEADER-->
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">	
 				<a class="brand" href="#">&nbsp;</a>
			
				<!--SET THE ACTIVE SECTION-->
				<ul class="nav" {$styeVar}>
					<li><a href="home.php?page=index" class="icon-home" value = >Home</a></li>
				</ul>
		
			
             <!--Top Right Tools-->
             <!-- <div id="top-right-tools">
           		<ul class="nav">	
					<li><a href="#"><img src="images/icon_runningbuilds.png" alt="Queue" title="Queue" /></a></li>
					<li class="dropdown"><a href="#" class="dropdown dropdown-toggle" data-toggle="dropdown" style="width:48px; height:48px; margin-top:0px; background-image:url('images/icon_user_gen.png');">
						<span class="caret" style="margin:16px 0 0 44px;"> </span>
						</a>
						<ul class="dropdown-menu pull-right">
                            <li>Not signed in</li>
                            <li class="divider"></li>
                            <li><button class="btn btn-primary" href="#">Log In</li>
	                    </ul>
					</li>
				</ul>
			</div> -->
			<!--Top Right Tools-->
			</div>
		</div>	
			
	</div>
	<!--HEADER-->

END;
	
	echo '<div id="main" class="hero-unit_yocto"><div class="row main" align="center"> ';
	echo '<br><p><h2><span style="color:blue;font-weight:bold">Enter your credentials:</span></h2></p><br><br><br>';
	echo '<table border="0">';
	echo '<tr><td><input type="text" placeholder="Email" style="width:190px;"></td>'; 
	echo '<td><p>Enter a valid email address<p></td></tr>';	
	echo '<tr><td><input type="text" placeholder="Username" style="width:190px;"></td>';
	echo '<td><p>Enter the name you will use to interact with others<p></td></tr>';
	echo '<tr><td><input type="text" placeholder="Password" style="width:190px;"></td>';
	echo '<td><p>Enter your password<p></td></tr>';
	echo '<tr><td><input type="text" placeholder="Picture" style="width:190px;"></td>';
	echo '<td><button onClick="addPictureToUser();" class="btn">Browse</button></td></tr>';
	echo '<tr> <td><br></td> <td><br></td> </tr>';
	echo '<tr> <td><br></td> <td><br></td> </tr>';
	echo '<tr><td><button onClick="addUser();" class="btn-primary btn">Submit</button></td>';		
	echo'<td><p><p></td></tr>';
	echo '</table></div></div>';
	
	/* <!-- Main Content--> */
	echo '<div id="main" class="container" ><div class="row"><div class="span4">';
	echo '<h2><span class="glyph enclosed">l</span> Community</h2>';
	echo '<p>The Yocto Project welcomes contributions from all types of people. You can contribute code, submit patches, file bugs, answer questions on our mailing lists and IRC channels, review and edit our documentation and much more!</p>';
	echo '<p>The Yocto community includes platform developers, equipment and device manufacturers and other embedded Linux professionals and enthusiasts. In short, people who are doing interesting work on Yocto. Our governance page has more information about the Yocto Project structure and how you can contribute.</p>';
	echo '<ul style="list-style-type:none;margin: 0 0 9px 0px;">';
	/*TODO: add correct links to the <a> tags */
	echo '<li><a href="#"><strong>Bugs &#187;</strong></a></li>';
	echo '<li><a href="#"><strong>IRC &#187;</strong></a></li>';
	echo '<li><a href="#"><strong>Mailing Lists &#187;</strong></a></li>';
	echo '<li><a href="#"><strong>Wiki &#187;</strong></a></li>';
	echo '<li><a href="#"><strong>Participating Organisations &#187;</strong></a></li></ul></div>';
	echo '<div class="span4"><h2><img src="images/icon_faq_36.png" alt="" /> FAQ</h2>';
	echo '<ul class="homefaq"><li><a href="#">What is the Yocto Project?</a></li>';
	
	/*TODO: add correct links to the <a> tags */
	echo '<li><a href="#">What does the Linux Foundation hope to achieve with the Yocto Project?</a></li>';
	echo '<li><a href="#">How does the Yocto Project help Linux reach a wider embedded audience?</a></li>';
	echo '<li><a href="#">Isn\'t the Yocto Project just yet another Linux distribution?';
	echo '<li><a href="#">What benefits does the Yocto Project provide embedded developers and how is the Yocto Project superior to existing, similar tools?</a></li>';
	echo '<li><a href="#">How does the existing embedded workflow compare to the Yocto Project and where can embedded developers save time?</a></li>';
	echo '<li><a href="#">Does the Yocto Project have a special governance model, or is it managed as an open source project?</a></li></ul></div>';
	
	/* TODO: add php crawler for this functionality and also correct links to the <a> tags */
	echo '<div class="span4"><h2><span class="glyph enclosed">l</span> Yocto News</h2><ul class="homenews">';
	echo '<li><a href="#"><strong>Documentation? They call it code for a reason.</strong></a> - Elizabeth Flanagan - 18 May 2012 - 10:54</li>';
	echo '<li><a href="#"><strong>Build Appliance: Eating Your Own Dog Food</strong></a> - Saul Wold - 7 May 2012 - 13:04</li>';
	echo '<li><a href="#"><strong>Yocto Project 1.2 Now Available</strong></a> - Jeffrey Osier-Mixon - 30 Apr 2012 - 17:11</li>';
	echo '<li><a href="#"><strong>A quick Yocto-grid</strong></a> - David Stewart - 24 Apr 2012 - 08:44</li>';
	echo '<li><a href="#"><strong>The Yocto-yumminess of our BSPs</strong></a> - David Stewart - 6 Apr 2012 - 16:21</li>';
	echo '<li><a href="#"><strong>More &raquo;</strong></a></li></ul></div> </div></div></div>';

echo <<< END
          
<footer>
	<div class="container" >
		<div class="row">
			<div class="span3" style="opacity:.65;">
				<p>&copy; 2012 The Yocto Project</p>
			</div>
			<div class="span3">
					<a href="#">About</a>
					<a href="#">Blogs</a>
					<a href="#">Documentation</a>
			</div>
			<div class="span3">
				<a href="#">Privacy Policy</a>
				<a href="#">Terms of Service</a>
				<a href="#">Trademarks</a>
			</div>
			<div class="span3">
				<a href="http://www.linuxfoundation.org" style="height:66px"><img src="images/linux_foundation.png" alt="Linux Foundation"/></a>
			</div>
		</div>
	</div>
</footer>

  </body>
</html>

END;

?>
