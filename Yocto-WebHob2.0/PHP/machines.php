<?php

echo <<< END

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- 
    <meta charset="utf-8">
    <title>Yocto Web Hob</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    -->

    <!-- Le styles -->
    <!-- 
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link href="css/yocto.css" rel="stylesheet">
    <link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">
    -->

    <!-- Le fav and touch icons -->
    <!-- 
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
    -->
     
  
     
  </head>

<body>

END;

	if (isset($_REQUEST['selectClass'])) {
		$selectClass = $_REQUEST['selectClass'];
	} else {
		$selectClass = "";
	}
    
    //FUTURE: add dynamic info from wikipedia
	
	//For the moment a static earch is done:	
	switch ($selectClass) 
	{
		case "qemuarm":
			echo '<p>The ARM architecture describes a family of RISC-based computer processors.Globally as of 2013 it is the most widely used 32-bit instruction set architecture in terms of quantity produced.<br />';
			echo '<a href="http://www.arm.com/">ARM website &#187;</a></p></div>';
			break;
		case "qemumips":
			echo '<p>MIPS (originally an acronym for Microprocessor without Interlocked Pipeline Stages) is a reduced instruction set computer (RISC). The early MIPS architectures were 32-bit, with 64-bit versions added later.<br />';
			echo '<a href="http://www.mips.com/">MIPS website &#187;</a></p></div>';
			break;
		case "qemuppc":
			echo '<p>PowerPC (short for Performance Optimization With Enhanced RISC – Performance Computing, sometimes abbreviated as PPC) is a RISC instruction set architecture. Originally intended for personal computers, PowerPC CPUs have since become popular as embedded and high-performance processors.<br />';
			break;
		case "qemux86-64":
			echo '<p>x86-64 (also known as x64, x86_64 and amd64) is the 64-bit version of the x86 instruction set. The original specification was created by AMD, and has been implemented by AMD, Intel, VIA, and others. It is fully backwards compatible with 16-bit and 32-bit x86 code.<br />';
			break;
		case "qemux86":
			echo '<p>x86 denotes a family of instruction set architectures based on the Intel 8086 CPU.The architecture has been implemented in processors from Intel, Cyrix, Advanced Micro Devices, VIA and many other companies.<br />';
			break;
		case "atom-pc":
			echo '<p>Intel Atom is the brand name for a line of ultra-low-voltage IA-32 and Intel 64 (x86-64) CPUs (or microprocessors) from Intel. Atom is mainly used in netbooks, nettops, embedded applications ranging from health care to advanced robotics, and mobile Internet devices (MIDs).<br />';
			echo '<a href="http://www.intel.com/content/www/us/en/processors/atom/atom-processor.html">Intel Atom website &#187;</a></p></div>';
			break;
		case "beagleboard":
			echo '<img src="images/logo_beagleboard.png" width="170" height="30" alt="BeagleBoard Logo" />';
			echo '<p>The BeagleBoard is a low-power open source hardware single-board computer produced by Texas Instruments in association with Digi-Key. It is sold to the public under the Creative Commons share-alike license.<br />';
			echo '<a href="http://beagleboard.org/">Beagleboard website &#187;</a></p></div>';
			break;
		case "mpc8315e-rdb":		
			echo '<p>The MPC8315 PowerPC reference platform (MPC8315E-RDB) is aimed at hardware and software development of network attached storage (NAS) and digital media server applications. The MPC8315E-RDB features the PowerQUICC II Pro processor, which includes a built-in security accelerator.<br />';
			break;
		case "routerstationpro":
			echo '<p>The RouterStation Pro is an enhanced version of the Ubiquiti RouterStation with a number of enhancements. <br />';
			echo '<a href="http://www.ubnt.com/rspro">RouterStation Pro website &#187;</a></p></div>';
			break;
		case "raspberrypi":
			echo '<img src="images/logo_raspberrypi.jpg" width="170" height="30" alt="RaspberryPi Logo" />';
			echo '<p>he Raspberry Pi is a credit-card-sized single-board computer developed in the UK by the Raspberry Pi Foundation with the intention of promoting the teaching of basic computer science in schools.<br />';
			echo '<a href="http://www.raspberrypi.org">Raspberry Pi website &#187;</a></p></div>';
			break;
		default:
			//FUTURE: add scropts that searches the machine name .conf file and prints the information from SUMMARY or DESCRIPTION
			echo '<p>Devices not recognized. No information available...<br />';
			break;
	}

echo <<< END

  </body>
</html>

END;

?>
