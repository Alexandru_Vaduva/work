<?php

if (!defined('projects_path')) define ('projects_path', '../XML/projects.xml');
if (!defined('users_path')) define ('users_path', '../XML/users.xml');
//if (!defined('groups_path')) define ('groups_path', '../XML/groups.xml');
if (!defined('templates_path')) define ('templates_path', '../XML/templates.xml');

if(!isset($_SESSION))
{
	session_start();
}  

//TODO: remove after project finished
error_reporting(E_ALL);
ini_set('display_errors', '1'); 

include_once "functions/project_functions.php";


//------------------------------------------------------------------------------
// Load and update XML related info
//------------------------------------------------------------------------------
$projects = simplexml_load_file(projects_path);
$users = simplexml_load_file(users_path);
//$groups = simplexml_load_file(groups_path);
$templates = simplexml_load_file(templates_path);

/* The project_name will be sent through a request form to the info page */
if (!defined('template_name')) define ('template_name', '../XML/Default Template.xml');
$templateName = simplexml_load_file(template_name);


				/* Projects */
if (!isset($_SESSION['projects_info']) OR ((int)$_SESSION['projects_iteration'] != (int)$projects->iteration))
{
   $_SESSION['projects_info'] = $projects->asXML();
   $_SESSION['projects_iteration'] = (int)$projects->iteration;
}
else
{
	$projects = simplexml_load_string($_SESSION['projects_info']);
}

				/* Users */
if (!isset($_SESSION['users_info']) OR ((int)$_SESSION['users_iteration'] != (int)$users->iteration))
{
   $_SESSION['users_info'] = $users->asXML();
   $_SESSION['users_iteration'] = (int)$users->iteration;
}
else
{
	$users = simplexml_load_string($_SESSION['users_info']);
}

				/* Groups */
/*if (!isset($_SESSION['groups_info']) OR ((int)$_SESSION['groups_iteration'] != (int)$groups->iteration))
{
   $_SESSION['groups_info'] = $groups->asXML();
   $_SESSION['groups_iteration'] = (int)$groups->iteration;
}
else
{
	$groups = simplexml_load_string($_SESSION['groups_info']);
}*/

				/* Templates */
if (!isset($_SESSION['templates_info']) OR ((int)$_SESSION['templates_iteration'] != (int)$templates->iteration))
{
   $_SESSION['templates_info'] = $templates->asXML();
   $_SESSION['templates_iteration'] = (int)$templates->iteration;
}
else
{
	$templates = simplexml_load_string($_SESSION['templates_info']);
}

				/* Template File Name */
if (!isset($_SESSION['templateName_info']) OR ((int)$_SESSION['templateName_iteration'] != (int)$templateName->iteration))
{
   $_SESSION['templateName_info'] = $templateName->asXML();
   $_SESSION['templateName_iteration'] = (int)$templateName->iteration;
}
else
{
	$templateName = simplexml_load_string($_SESSION['templateName_info']);
}

//------------------------------------------------------------------------------
// Requests related info
//------------------------------------------------------------------------------
//page request
if (isset($_REQUEST['page']))
{
	$page = $_REQUEST['page'];
}
else
   $page = "index";
echo "Pagina: ".$page."<br>";

$projectName = $projects->selected;
if (isset($_REQUEST['project']))
{
	$projectName = $_REQUEST['project'];
	$projects->selected = $projectName;	
	
	//Update $_SESSION  and save project info.
	$_SESSION['projects_iteration'] += 1;
	$_SESSION['projects_info'] = $projects->asXML();
	$projects->asXML('../XML/projects.xml');
}
$location = '../XML/'.$projectName.'.xml';
if (!empty($projectName) && strcmp($projectName, " ") != 0)
{
	if (!defined('project_name')) define ('project_name', $location);
	$projName = simplexml_load_file(project_name);
		
					/* Project File Name */
	$projName = simplexml_load_string($projName->asXML());
}
echo "Project: ".$projectName."<br>";

//build request
$request = "qemux86";
if (isset($_REQUEST['build']))
{
	$request = $_REQUEST['build'];
	
	//------------------------------------------------------------------------------
	// Server related info
	//------------------------------------------------------------------------------
	$fp = fopen('/home/alexandru/workspace/work/Yocto-WebHob2.0/python/image.txt', 'w');
	fwrite($fp, $request);
}
echo "Build: ".$request."<br>";


//------------------------------------------------------------------------------
// Session related info
//------------------------------------------------------------------------------
//existing session, check if logout requested
if (isset($_SESSION['started']))
{
	//echo 'existing session: ';
	//check if logout requested or user disabled/deleted
    if (!checkUserEnabled()) 
    {
        removeSession(session_id());
        unset($_SESSION['started']);
        $page = "index_dashboard";
    }
    else 
    {
        $_SESSION['started'] = time();
        updateSession(session_id());
    }
}

//new session, either new connection, after logout
if (!isset($_SESSION['started']))
{
    $numSessions = getNumSessions();
    //echo $numSessions;
    if ($numSessions < 1000000) 
    {
        $_SESSION['started'] = time();
        $_SESSION['loggedin'] = 0;
        //TODO: add the main pages that require configuration changes if needed
        //$_SESSION['builds'] = 0; etc....    
        addSession(session_id());
    } 
    else 
    {
        session_destroy();
        echo "Maximum number of contemporary session reached, please try again later";
        die();
    }
}

//------------------------------------------------------------------------------
// Login related info
//------------------------------------------------------------------------------
//add login properties
if (isset($_REQUEST['password']) && isset($_REQUEST['email']))
{
	$email = trim($_REQUEST['email']);
    $password = trim($_REQUEST['password']);
	doLogin($password, $email);
}

//------------------------------------------------------------------------------
// Content related info
//------------------------------------------------------------------------------
//if the user does not login on the page he will not have acess to the other pages
//Add every page to a check information.
$pages = array( "builds"=>"builds",
				"createbuild_buildimage_deploy"=>"createbuild_buildimage_deploy",
				"createbuild_buildimage_download"=>"createbuild_buildimage_download",
				"createbuild_buildimage"=>"createbuild_buildimage",
				"createbuild_buildimage_test"=>"createbuild_buildimage_test",
				"createbuild_packagegroups"=>"createbuild_packagegroups",
				"createbuild_packages"=>"createbuild_packages",
				"createbuild_recipes"=>"createbuild_recipes",
				"home"=>"home",
				"index_dashboard"=>"index_dashboard",
				"index"=>"index",
				"projects_history"=>"projects_history",
				"projects_myproject"=>"projects_myproject",
				"projects_permissions"=>"projects_permissions",
				"projects"=>"projects",
				"projects_settings"=>"projects_settings",
				"sing_up"=>"sing_up");

if (!$_SESSION['loggedin'])
{
	if (strcmp($pages[$page], "sing_up") == 0)
		include_once $pages[$page].".php";
	else
		if (isset ($pages[$page])) 
			include_once $pages["index"].".php";
}
else
{
	if (isset ($pages[$page])) 
		include_once $pages[$page].".php";
}

?>
