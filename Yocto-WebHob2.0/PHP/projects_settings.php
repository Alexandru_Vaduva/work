<?php
/*
error_reporting(E_ALL); 
ini_set('display_errors','1'); 

include "test.php";

global $projName;
*/
$selections = array("jffs2", "sum.jffs2", "cramfs", "ext2", "ext2.gz", "ext2.bz2",
					"ext3", "ext3.gz", "ext2.lzma", "btrfs", "live", "squashfs",
					"squashfs.lzma", "ubi", "tar", "tar.gz", "tar.bz2", "tar.xz",
					"cpio", "cpio.gz", "cpio.xz", "cpio.lzma", "vmdk", "elf");
$packages = array("rpm", "deb", "ipk");
$toolchains = array("i586", "i686", "x86_64");
$build_envs = array("defaultsetup", "poky", "poky-bleeding", "poky-lsb", "poky-tiny");

echo <<< END

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Yocto Web Hob</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
<!--remove and use @import in css in production-->
<link href="../assets/css/bootstrap.css" rel="stylesheet">
<!--remove and use @import in css in production-->
<link href="css/yocto.css" rel="stylesheet">
<link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">
<!--[if !IE 7]>
<style type="text/css">
#wrap {display:table;height:100%}
</style>
<![endif]-->

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="../assets/ico/favicon.ico">

</head>

<body>

<div id="wrap">

	<!--HEADER-->
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">	
 				<a class="brand" href="home.php?page=index_dashboard">&nbsp;</a>
			
				<ul class="nav" style="text-align:center;">
					<!--SET THE ACTIVE SECTION by adding class="active"-->
					<li><a href="home.php?page=builds" class="icon-builds">Builds</a></li>
					<li class="active"><a href="home.php?page=projects" class="icon-projects">Projects</a></li>
					<!-- <li><a href="home.php?page=groups" class="icon-groups">Groups</a></li> -->
				</ul>
		
			
             <!--Top Right Tools-->
             <div id="top-right-tools">
           		<ul class="nav">	
					<li style="visibility:hidden;"><a href="#"><img src="images/icon_search.png" alt="Search" title="Search" /></a></li>
					<li><a data-toggle="modal" href="#queueModal">
						<span class="badge badge-success" style="float:right;margin-left:-2px;">2</span>
						<img src="images/icon_runningbuilds.png" alt="Queue &mdash; 2 builds in progress" title="Queue &mdash; 2 builds in progress" />
						</a></li>
					<li class="dropdown"><a href="#" class="dropdown dropdown-toggle" data-toggle="dropdown" style="width:48px; height:48px; margin-top:-8px; background-image:url('images/icon_user.png');">
						<span class="caret"> </span>
						</a>
						<ul class="dropdown-menu pull-right">
                            <li><img src="images/icon_user.png">&nbsp; <strong>John Doe</strong></li>
                            <li class="divider"></li>
                            <li><a href = "#">Settings</a></li>
                            <li><a href = "#">Account details</a></li>
                            <li class="divider"></li>
                            <li><button class="btn btn-primary" href="home.php?page=index">Log Out</button></li>
	                    </ul>
					</li>
				</ul>
			</div>
			<!--Top Right Tools-->
			</div>
		</div>	
			
	</div>
	<!--HEADER-->
	
END;

	//TODO: add alert messages only when needed. For the moment they are not used.
	echo '<div id="main" class="container" >';
	/*echo '<div class="alert alert-success alert-block">';
	echo '<a class="close" data-dismiss="alert" href="#">&times;</a>';
	echo '<p class="alert-heading"><strong>Saved! You successfully read this important alert message.</strong></p></div>';*/
	
	echo '<!-- Main Content --><!--Top Bar--><div class="row"><div class="span12" >';
	echo '<h1><span class="glyph enclosed" style="vertical-align:top;">c</span> Projects</h1>';
	
	//TODO: add name of the project sellected through form requests
	echo '<h2 class="pull-left">'.$projName->setup->name.'</h2>';
	
	echo '<ul class="nav nav-pills topbarnav pull-left" style="margin-left:0;">';
	echo '<li><a href="home.php?page=projects_myproject">Project</a></li>';
	echo '<li><a href="home.php?page=projects_history"><i class="icon-time icon-blue"></i> History</a></li>';
	echo '<li class="active"><a href="home.php?page=projects_settings"><i class="icon-cog icon-white"></i> Settings</a></li></ul></div>';
	//echo '<li class="active"><a href="home.php?page=projects_settings"><i class="icon-cog icon-white"></i> Settings</a></li>>';
	//echo '<li><a href="home.php?page=projects_permissions"><i class="icon-lock icon-blue"></i> Permissions</a></li></ul></div>';
	
	echo '</div><!--Top Bar--><hr /><div class="row"><!--MAIN WORK AREA--><div class="span9">';
	echo '<h3>Output</h3><h4>Image Types</h4><form><div class="control-group checkbox_series"><table class="settingschecks">';
	
	$nr_options = sizeof($selections);
	$nr_divisions = $nr_options / 4;
	for ($i = 0; $i < $nr_options; )
	{
		echo '<tr>';
		if ($i < $nr_divisions * 4)
		{
			for ($j = 0; $j < 4; $j++)
			{
				echo '<td><label class="checkbox">';
				$check = " ";
				if (strcmp($projName->setup->settings->image_type, $selections[$i]) == 0)
					$check = " checked";
				echo '<input type="checkbox"'.$check.'> '.$selections[$i].'</label></td>';
				$i++;
				if ($i >= $nr_options) break;
			}
		}
		/*NO need to use this part. For the moment there are 24 image types
		 * else
		{
			for ($j = $i; $j < $nr_options; $j++)
			{
				echo '<td><label class="checkbox">';
				$check = " ";
				if (strcmp($projName->setup->settings->image_type, $selections[$i]) == 0)
					$check = " checked";
				echo '<input type="checkbox"'.$check.'> '.$selections[$i].'</label></td>';
				$i++;
				if ($i >= $nr_options) break;
			}
		}*/
		echo '</tr>';
	}
	echo '</table></div><!--Packaging Format--><div class="row"><div class="span2">';
	echo '<h4>Packaging Format</h4></div><div class="span7"><div class="row"><div class="span3">';
	echo '<label class="control-label" for="select01">Root file system packaging format</label>';
	echo '<div class="controls"><select id="select01">';
	
	$nr_packages = sizeof($packages);
	for ($i = 0; $i < $nr_packages; $i++)
	{
		$select = " ";
		if (strcmp($projName->setup->settings->package_format, $packages[$i]) == 0)
			$select = " selected";
		echo '<option'.$select.'>'.$packages[$i].'</option>';
	}
	echo '</select></div></div><div class="span4"><p>Selects the package format used to generate rootfs.</p></div></div>';	
	//The aditional formats remain as they are. Maybe the form of the option "Packaging Format" will change.
	echo '<div class="row"><div class="span3"><label>Additional package formats</label>';
	echo '<label class="checkbox"><input type="checkbox"> deb</label>';
	echo '<label class="checkbox"><input type="checkbox"> ipk</label></div>';
	echo '<div class="span4"><p>Selects extra package formats to build.</p></div>';
	echo '</div></div></div><!--Packaging Format-->';
	
	echo '<!--Image rootfs size--><div class="row"><div class="span2"><h4>Image rootfs size</h4></div>';
	echo '<div class="span7"><div class="row"><div class="span3">';
	echo '<input type="text" class="input-small pull-left" placeholder="'.$projName->setup->settings->rootfs_size.'"> &nbsp;<label class="pull-left" style="margin: 5px 0 0 5px;"> MB</label></div>';
	echo '<div class="span4"><p>Sets the basic size of your target image. This is the basic size of your target image unless your selected package size exceeds this value or you select Image Extra Size.</p>';
	echo '</div></div></div></div><!--Image rootfs size-->';
	
	echo '<!--Image extra size--><div class="row"><div class="span2"><h4>Image extra size</h4></div>';
	echo '<div class="span7"><div class="row"><div class="span3">';
	echo '<input type="text" class="input-small pull-left" placeholder="'.$projName->setup->settings->extra_size.'"> &nbsp;<label class="pull-left" style="margin: 5px 0 0 5px;"> MB</label></div>';
	echo '<div class="span4"><p>Sets the extra free space of your target image. By default, the system reserves 30% of your image size as free space. If your image contains zypper, it brings in 50MB more space. The maximum free space is 64MB.</p>';
	echo '</div></div></div></div><!--Image extra size--><hr />';
	echo '<div class="row"><div class="span3"><label class="checkbox">';
	$exclude_GPLv3 = " ";
	if ($projName->setup->settings->gpl3_packages == 0)
			$exclude_GPLv3 = " checked";
	echo '<input type="checkbox"'.$exclude_GPLv3.'> Exclude GPLv3 packages</label>';
	$toolchain = " ";
	if (!empty($projName->setup->settings->toolchain) && strcmp($projName->setup->settings->toolchain, " ") != 0)
		$toolchain = " checked";
	echo '<label class="checkbox"><input type="checkbox"'.$toolchain.'> Build toolchain</label>';	
	echo '<select id="select02">';
	$nr_toolchains = sizeof($toolchains);
	for ($i = 0; $i < $nr_toolchains; $i++)
	{
		$select = " ";
		if (strcmp($projName->setup->settings->toolchain, $toolchains[$i]) == 0)
			$select = " selected";
		echo '<option'.$select.'>'.$toolchains[$i].'</option>';
	}
	
	echo '</select></div><div class="span4">';
	echo '<p>Selects the host platform for which you want to run the toolchain.</p></div></div><hr />';
	
	echo '<h3 style="margin-top: 18px;">Build Environment</h3><div class="row"><div class="span3"><label>Select distro</label><select id="select03">';
	$nr_envs = sizeof($build_envs);
	for ($i = 0; $i < $nr_envs; $i++)
	{
		$select = " ";
		if (strcmp($projName->setup->settings->distro, $build_envs[$i]) == 0)
			$select = " selected";
		echo '<option'.$select.'>'.$build_envs[$i].'</option>';
	}
	echo ' </select></div><div class="span4"><p>&nbsp;</p><p>Select the best distro.</p></div></div>';
	echo '</form></div><!--MAIN WORK AREA-->';
     
    echo '<!--SIDEBAR--><div class="span3"><div>';
	echo '<p><button class="btn">Save settings as template</button></p>';
	echo '<p><button class="btn">Archive project</button></p>';
	echo '<p><a href="#"><i class="icon-trash icon-blue"></i> Delete project</a></p></div><p>&nbsp;</p>';
	echo '<div class="well"><h4>Settings and Projects</h4>';
	echo '<p>This is a very specific section, very advanced settings. From here the user can select the output for the packages built and different other properties. There is also the possibility to save a template for later use of the properties that were set here.</p>';
	echo '<p>For space reduction there is the posibility to delete or archive. This is done generally after the work with the project is done.</p>';
	echo '<p><a href="http://www.yoctoproject.org/docs/current/yocto-project-qs/yocto-project-qs.html"><strong>More about build properties &raquo;</strong></a></p></div></div><!--SIDEBAR--></div></div><!--Main Content-->';
	
echo <<< END

<!--Queue Modal Content-->
<div id="queueModal" class="modal hide">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h3>Running Builds</h3>
            </div>
            <div class="modal-body">
              <h4>Build 1</h4>
              <p>Some information about the build.</p>
              
              <div class="progress progress-success">
                    <div class="bar" style="width: 60%;"></div>
              </div>
              
              <div class="alert alert-success">Done! <a href="#">Download Build 1 here.</a></div>

              <h4>Build 2</h4>
              <p>Some information about the build.</p>
              
              <div class="progress progress-striped
     active">
                    <div class="bar " style="width: 60%;"></div>
              </div>
              
              <p><i class="icon-time"></i> 01:23:02 remaining</p>

              <h4>Build 3</h4>
              <p>Some information about the build.</p>

              <div class="progress progress-striped
     active">
                    <div class="bar " style="width: 60%;"></div>
              </div>
              
              <p><i class="icon-time"></i> 01:23:02 remaining</p>

              
            </div>
            <div class="modal-footer">
              <a href="#" class="btn" data-dismiss="modal" >Close</a>
            </div>
          </div>
<!--Queue Modal Content-->

    </div><!-- /container -->


	<footer>
		<div class="container" >
			<div class="row">
				<div class="span3" style="opacity:.65;">
					<p>&copy; 2012 The Yocto Project</p>
				</div>
				<div class="span3">
						<a href="#">About</a>
						<a href="#">Blogs</a>
						<a href="#">Documentation</a>
				</div>
				<div class="span3">
					<a href="#">Privacy Policy</a>
					<a href="#">Terms of Service</a>
					<a href="#">Trademarks</a>
				</div>
				<div class="span3">
					<a href="http://www.linuxfoundation.org" style="height:66px"><img src="images/linux_foundation.png" alt="Linux Foundation"/></a>
				</div>
			</div>
		</div>
	</footer>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap-transition.js"></script>
    <script src="../assets/js/bootstrap-alert.js"></script>
    <script src="../assets/js/bootstrap-modal.js"></script>
    <script src="../assets/js/bootstrap-dropdown.js"></script>
    <script src="../assets/js/bootstrap-scrollspy.js"></script>
    <script src="../assets/js/bootstrap-tab.js"></script>
    <script src="../assets/js/bootstrap-tooltip.js"></script>
    <script src="../assets/js/bootstrap-popover.js"></script>
    <script src="../assets/js/bootstrap-button.js"></script>
    <script src="../assets/js/bootstrap-collapse.js"></script>
    <script src="../assets/js/bootstrap-carousel.js"></script>
    <script src="../assets/js/bootstrap-typeahead.js"></script>


  </body>
</html>

END;

?>
