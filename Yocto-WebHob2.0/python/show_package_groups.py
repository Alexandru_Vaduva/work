#!/usr/bin/python

import subprocess, ntpath, os

def path_leaf(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)

# package groups are the one from the image file that the user choses. Example "core-image-minimal"
##Correction: for package groups info is at the link: http://www.yoctoproject.org/docs/1.4/dev-manual/dev-manual.html
    ## the section you are looking for is called: "Customizing Images Using Custom Package Groups"
cmdline = ["cd /home/alexandru/workspace/yocto/poky", "find ./ -name \"packagegroup*.bb\""]	
proc  = subprocess.Popen(";".join(cmdline), shell=True, executable='/bin/bash', stdout=subprocess.PIPE) 
result = proc.stdout.read()

lines = filter(None, result.split('\n'))
output = []
backup = []

for line in lines:
    backup.append(path_leaf(line))
    
for aux in backup:
    output.append(aux.split('.')[0].split('packagegroup-')[1])
print output

#output should be similar to this one.
#output = ["apps-console-core", "x11-mini", "x11-base", "x11-sato", "apps-x11-core", "apps-x11-games", "tools-sdk", "tools-debug", "tools-profile", "tools-testapps", "nfs-server", "ssh-server-dropbear", "ssh-server-openssh", "debug-tweaks"]
