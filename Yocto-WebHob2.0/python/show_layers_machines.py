#!/usr/bin/python

import subprocess, os


# Open file that has build directory information. 
fi = open('/home/alexandru/workspace/work/Yocto-WebHob2.0/python/build.txt', 'r')
lines = fi.readlines()
fi.close()

data = lines[0].split('\n')[0]
buildDir = "/home/alexandru/workspace/yocto/" + data + "/conf/bblayers.conf";
bbf = open(buildDir, 'r')
lines = bbf.readlines()

found = False
layers = []
for line in lines:
	if line.find("BBLAYERS") != -1:
		found = True
		continue
	if found == True:
		#aux = line.split(' \\')[0]
		layers.append(line.split(' ')[2])

layers.pop()
#print layers
bbf.close()

machines = []
for layer in layers:
	cmdline = []
	cmd = "cd " + layer + "/conf/machine";
	cmdline.append(cmd)
	cmdline.append("ls")
	proc  = subprocess.Popen(";".join(cmdline), shell=True, executable='/bin/bash', stdout=subprocess.PIPE)
	#result = proc.stdout.read()
	
	outputlines = filter(lambda x:len(x)>0,(line.strip() for line in proc.stdout))
	#print outputlines
	
	for result in outputlines:
		if result.find(".conf") != -1:
			machines.append(result.split('.')[0])
	
print machines
