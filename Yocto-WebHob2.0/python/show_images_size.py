#!/usr/bin/python

import os, subprocess


SYMBOLS = ('K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')
PREFIX = {}
 
for i, s in enumerate(SYMBOLS):
	PREFIX[s] = 1 << (i+1)*10
 
def convert_bytes(n):
	for s in reversed(SYMBOLS):
		if n >= PREFIX[s]:
			value = float(n) / PREFIX[s]
			return '%.1f%s' % (value, s)
	 
	# n is less than 1024B
	return '%.1fB' % n

# packages are the one in 'tmp/deploy/images' that are used by the board. There are the ones makerd by the symbolic link in this loccation (l)
cmdline = ["cd /home/alexandru/workspace/yocto/testBuild/tmp/deploy/images", "ls -lh `find ./ -maxdepth 1 -type l -print`"]	
proc  = subprocess.Popen(";".join(cmdline), shell=True, executable='/bin/bash', stdout=subprocess.PIPE)
output = proc.stdout.read()

bases_size = []
substrs = ["uImage", "zImage"]
lines = filter(None, output.split('\n'))
for line in lines:
	rootfs = True
	result = line.split('->')[0].split('./')[1]
	for substr in substrs:
		if substr in result:
			rootfs = False
			break
	location = "/home/alexandru/workspace/yocto/testBuild/tmp/deploy/images/"+result.split(' ')[0]
	if rootfs == True:
		#print convert_bytes(os.path.getsize(location))
		bases_size.append(convert_bytes(os.path.getsize(location)))

print bases_size
