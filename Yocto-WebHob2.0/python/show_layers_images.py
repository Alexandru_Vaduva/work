#!/usr/bin/python

import subprocess, os


# Open file that has build directory information. 
fi = open('/home/alexandru/workspace/work/Yocto-WebHob2.0/python/build.txt', 'r')
lines = fi.readlines()
fi.close()

data = lines[0].split('\n')[0]
buildDir = "/home/alexandru/workspace/yocto/" + data + "/conf/bblayers.conf";
bbf = open(buildDir, 'r')
lines = bbf.readlines()

found = False
layers = []
for line in lines:
	if line.find("BBLAYERS") != -1:
		found = True
		continue
	if found == True:
		layers.append(line.split(' ')[2])

layers.pop()
#print layers
bbf.close()

images = []
for layer in layers:
	cmdline = []
	cmd = "find " + layer + " -name \"*image*.bb\"";
	cmdline.append(cmd)
	proc  = subprocess.Popen(";".join(cmdline), shell=True, executable='/bin/bash', stdout=subprocess.PIPE)
	#result = proc.stdout.read()
	
	outputlines = filter(lambda x:len(x)>0,(line.strip() for line in proc.stdout))
	#print outputlines
	
	for result in outputlines:
		res = result.split('.')[0].split('/')[9]
		#print res
		images.append(res)
	
print images
