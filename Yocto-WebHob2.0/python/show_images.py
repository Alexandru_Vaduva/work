#!/usr/bin/python

import subprocess

# packages are the one in 'tmp/deploy/images' that are used by the board. There are the ones marked by the symbolic link(l) in this location
cmdline = ["cd /home/alexandru/workspace/yocto/testBuild/tmp/deploy/images", "ls -l `find ./ -maxdepth 1 -type l -print`"]	
proc  = subprocess.Popen(";".join(cmdline), shell=True, executable='/bin/bash', stdout=subprocess.PIPE)
output = proc.stdout.read()

bases = []
substrs = ["uImage", "zImage"]
lines = filter(None, output.split('\n'))
for line in lines:
	rootfs = True
	result = line.split('->')[0].split('./')[1]
	for substr in substrs:
		if substr in result:
			rootfs = False
			break
	if rootfs == True:
		bases.append(result)

print bases
