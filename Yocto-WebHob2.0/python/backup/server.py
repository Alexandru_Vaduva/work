#!/usr/bin/python

import socket, subprocess


HOST = 'localhost'    # The remote host
PORT = 50003          # The same port as used by the server

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)

errors = []
conn, addr = s.accept()

f = open('/home/alexandru/workspace/work/Yocto-WebHob2.0/python/image.txt', 'r')
lines = f.readlines()
f.close()
#print lines

#output = "rpi-basic-image"
output = lines[0].split('\n')[0]
#print repr(output)
conn.send(output)

while True:
	in_txt = conn.recv(1024)
	#print in_txt
	if in_txt == "VJA_WebHob_Yocto":
		conn.close() 
		break
	errors.append(in_txt)
    
#print errors     
