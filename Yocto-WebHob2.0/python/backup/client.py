#!/usr/bin/python

import socket, subprocess


HOST = 'localhost'    # The remote host
PORT = 50003          # The same port as used by the server

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

data = s.recv(1024)
#print 'Received', repr(data)

cmdline = ["cd /home/alexandru/workspace/yocto/", "source poky/oe-init-build-env YoctoBlockRollout/"]
bitbake = "bitbake " + data;
cmdline.append(bitbake)
#print cmdline
proc  = subprocess.Popen(";".join(cmdline), shell=True, executable='/bin/bash', stdout=subprocess.PIPE)
result = proc.stdout.read()

f = open('/home/alexandru/workspace/work/Yocto-WebHob2.0/python/bitbake.txt', 'w')
f.writelines(result)
f.close()

errors = []
lines = filter(None, result.split('\n'))
for line in lines:
    if line.find("ERROR") != -1:
        #print line
        errors.append(line)
        s.send(line)
    
finish = "VJA_WebHob_Yocto"             
s.send(finish)
s.close()

print errors



