#!/usr/bin/python

import subprocess, os
from lxml import etree as ET


buildDir = "/home/alex/workspace/yocto";
cmdline = "find "+buildDir+" -maxdepth 1 -type d -print";

proc  = subprocess.Popen(cmdline, shell=True, executable='/bin/bash', stdout=subprocess.PIPE)
output = proc.stdout.read()

lines = filter(None, output.split('\n'))
baseline = lines[0]+"/"
results = []
#Remove the baseline from directories and also the poky directory
for line in lines:
	if line in [baseline, lines[0], "poky"]:
		continue
	else:
		if not (line.replace(baseline, '') in ["poky"]):
			results.append( line.replace(baseline, '') )
	
#We need to go through all the results and complete the xml files with corresponding data.
#Functionality for groups and templates can dissapear for this version of the project
projects = ET.Element("projects")
selected = ET.SubElement(projects, "selected").text = results[0]
list = ET.SubElement(projects, "list")

for res in results:
	plocate = buildDir+"/"+res+"/README"
	if os.path.exists(plocate):
		with open(plocate) as readme_file:
			for line in readme_file:
				if "Description:" in line:
					pinfo = line.replace('Description: ', '').replace('\n', '')
					break
	item = ET.SubElement(list, "item")
	name = ET.SubElement(item, "name").text = res
	info = ET.SubElement(item, "info").text = pinfo
	location = ET.SubElement(item, "location").text = plocate

tree = ET.ElementTree(projects)
#print(ET.tostring(projects, pretty_print=True))
tree.write("projects.xml", pretty_print=True)

for res in results:
	projectName = ET.Element(res)
	setup = ET.SubElement(projectName, "setup")
	projName = ET.SubElement(setup, "name").text = res

	plocate = buildDir+"/"+res+"/conf/bblayers.conf"
	layers = []
	requirement = 0
	reqLayers = []
	if os.path.exists(plocate):
		with open(plocate) as readme_file:
			for line in readme_file:
				pinfo = line.replace('\n', '').replace(' \\', '')
				if "BBLAYERS" in pinfo:
					requirement = 1
				if "BBLAYERS_NON_REMOVABLE" in pinfo:
					requirement = 2
				if buildDir in pinfo:
					if requirement == 1:
						layers.append( pinfo.replace(' ', '') )
					if requirement == 2: 
						reqLayers.append( pinfo.replace(' ', '') )
	
	players = ET.SubElement(setup, "layers")
	for layer in layers:
		item = ET.SubElement(players, "item")
		lName = ET.SubElement(item, "name").text = layer.replace(buildDir+"/poky/", '')
		occupation = "Test layer"
		for reqLayer in reqLayers:
			if layer == reqLayer:	
				occupation = "Required"
				break
		utility = ET.SubElement(item, "utility").text = occupation
	
	plocate = buildDir+"/"+res+"/conf/local.conf"
	if os.path.exists(plocate):
		with open(plocate) as readme_file:
			for line in readme_file:
				if "#" in line:
					if "#DL_DIR" in line:
						pinfo = line.replace('#DL_DIR ?= ', '').replace('"', '')
						dlDir = pinfo.replace('\n', '').replace('${TOPDIR}', buildDir+"/"+res)
					if "#SSTATE_DIR" in line:
						pinfo = line.replace('#SSTATE_DIR ?= ', '').replace('"', '')
						sstateDir = pinfo.replace('\n', '').replace('${TOPDIR}', buildDir+"/"+res)
					if "#TMPDIR" in line:
						pinfo = line.replace('#TMPDIR = ', '').replace('"', '')
						tmpDir = pinfo.replace('\n', '').replace('${TOPDIR}', buildDir+"/"+res)
					if "#SDKMACHINE" in line:
						pinfo = line.replace('#SDKMACHINE ?= ', '').replace('"', '')
						sdkMachine = pinfo.replace('\n', '')
				else:
					if "MACHINE" in line:
						pinfo = line.replace('MACHINE ?= ', '').replace('"', '')
						machine = pinfo.replace('\n', '')
					if "DISTRO" in line:
						pinfo = line.replace('DISTRO ?= ', '').replace('"', '')
						distro = pinfo.replace('\n', '')
					if "PACKAGE_CLASSES" in line:
						pinfo = line.replace('PACKAGE_CLASSES ?= ', '').replace('"', '')
						packageFormat = pinfo.replace('\n', '').replace('package_', '')
	
	settings = ET.SubElement(setup, "settings")
	machine = ET.SubElement(settings, "machine").text = machine
	distro = ET.SubElement(settings, "distro").text = distro
	packageFormat = ET.SubElement(settings, "package_format").text = packageFormat
	sdkMachine = ET.SubElement(settings, "sdk_machine").text = sdkMachine
	dlDir = ET.SubElement(settings, "download_dir").text = dlDir
	sstateDir = ET.SubElement(settings, "sstate_dir").text = sstateDir
	tmpDir = ET.SubElement(settings, "tmp_dir").text = tmpDir
	
	tree = ET.ElementTree(projectName)
	projectFileName = res+".xml"
	tree.write(projectFileName, pretty_print=True)
	
#OBS: not the best version because:
#	-	it offers usefull information to the user: possible security breach
#	-	does not gather information efficiently from local.conf folder.
