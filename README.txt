					README file
					===========

Content:
	General information
	Progress activity
	TODO section
	DONE section


				General information
				===================

WebHob design starting point:
	https://github.com/jkosem/Yocto-WebHob-Design
	http://git.yoctoproject.org/cgit/cgit.cgi/yocto-webhob-design/
For Windows testing "USBWebserver" can be used:
	http://www.usbwebserver.net/en/download.php
WebHob design uses the Bootstrap framework:
	http://twitter.github.com/bootstrap/
Update information for the 2.0 WebHob can be found here:
	https://wiki.yoctoproject.org/wiki/Yocto_1.3_Web_Hob
Run application using one of the two links:
	http://localhost:8080/Yocto-WebHob2.0/PHP/
	http://localhost/Yocto-WebHob2.0/PHP/

				Progress activity
				=================

Project setup:
	$> sudo apt-get install apache2 php5 libapache2-mod-php5
	$> sudo /etc/init.d/apache2 restart
	$> git clone https://Alexandru_Vaduva@bitbucket.org/Alexandru_Vaduva/work.git
	$> chmod -R 777 work
	$> sudo ln -s Yocto-WebHob2.0/ /var/www/WebHob
		or
	$> sudo vim /etc/apache2/sites-available/default
		#DocumentRoot /var/www
	     DocumentRoot /home/alex/workspace/work

Geany setup:
	$> git clone https://github.com/trongthanh/geany-for-front-end-dev.git
	$> cd geany-for-front-end-dev/
	$> chmod +x install.sh
	$> ./install.sh
	$> sudo apt-get install geany-plugin-webhelper geany-plugin-xmlsnippets
	$> sudo apt-get install geany-plugins geany-plugins-common
	$> sudo apt-get install geany-plugin-spellcheck

Installed simplexml library for XML parsing functionality in python:
    $> sudo apt-get install python-pip
    $> pip install python-simplexml
    $> pip install --upgrade Scrapy


                TODO section
				============

1. Change the name of the product from WebHob to something bold and catchy.
2. Remove the groups front-end functionality from the product. (DONE)
3. Remove the groups back-end functionality from the product. (DONE)
4. Remove project permission section. (DONE)
5. Update build_image.py functionality and offer continuous build progress update info.
    - Progress = build start time + T * build tasks number, where T is an ETA for  task.
        OBS: Ex.: Fetch task can be done in parallel so if a sufficient thread number is given, until the do_configure
            task  a big number of tasks are done. So be carefull on the ETA and T calculation.
    - More professional is to inspect the bitbake sources and enable "pv" tool  functionality. Maybe the best solution,
        still needs source code inspection.
6. Check show layers. It shows the layers available in poky directory. It does not seem to be used
7. Update all python scripts and remove all static definitions.
8. Add a build script that sets environment variables so that the setting of the variables in the
	python scripts will be done automatically.

                Targets
                =======

1. Produce an automate script that offers informations about the environment:
    - Inspact bitbake -e / bitbake -g
2. Find a way to permit source interaction from web page client, with minimum to none security problems.
    - xmmp / jabber study
3. Find a method to offer an estimate for a build.
    - git clone bitbake sources
